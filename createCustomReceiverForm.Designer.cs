﻿using iMS.Resource;
using iMS.Classes;
using System;
using System.Windows.Forms;
namespace iMS
{
  partial class createCustomReceiverForm
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.CCRBox = new System.Windows.Forms.GroupBox();
      this.CCRPanel = new System.Windows.Forms.Panel();
      this.DeleteButton = new System.Windows.Forms.Button();
      this.SaveButton = new System.Windows.Forms.Button();
      this.MembersOfRecipientsLabel = new System.Windows.Forms.Label();
      this.MembersOfRecipientsValue = new System.Windows.Forms.TextBox();
      this.ListNameValue = new System.Windows.Forms.TextBox();
      this.ListName = new System.Windows.Forms.Label();
      this.SavedListValue = new System.Windows.Forms.ComboBox();
      this.SavedListLabel = new System.Windows.Forms.Label();
      this.erProvider = new System.Windows.Forms.ErrorProvider();
      ((System.ComponentModel.ISupportInitialize)(this.erProvider)).BeginInit();
      this.SuspendLayout();
      this.CCRBox.SuspendLayout();
      this.CCRPanel.SuspendLayout();
      this.SuspendLayout();
      // 
      // CCRBox
      // 
      this.CCRBox.Controls.Add(this.CCRPanel);
      this.CCRBox.Location = new System.Drawing.Point(5, 5);
      this.CCRBox.Name = "CCRBox";
      this.CCRBox.Size = new System.Drawing.Size(282, 256);
      this.CCRBox.Text = ResourceManager.Resource.GetString(Constants.WORDS.XXIV);
      // 
      // CCRPanel
      // 
      this.CCRPanel.Controls.Add(this.DeleteButton);
      this.CCRPanel.Controls.Add(this.SaveButton);
      this.CCRPanel.Controls.Add(this.MembersOfRecipientsLabel);
      this.CCRPanel.Controls.Add(this.MembersOfRecipientsValue);
      this.CCRPanel.Controls.Add(this.ListNameValue);
      this.CCRPanel.Controls.Add(this.ListName);
      this.CCRPanel.Controls.Add(this.SavedListValue);
      this.CCRPanel.Controls.Add(this.SavedListLabel);
      this.CCRPanel.Location = new System.Drawing.Point(3, 14);
      this.CCRPanel.Name = "CCRPanel";
      this.CCRPanel.Size = new System.Drawing.Size(276, 239);
      // 
      // DeleteButton
      // 
      this.DeleteButton.Cursor = System.Windows.Forms.Cursors.Hand;
      this.DeleteButton.Image = global::iMS.Properties.Resources.Delete;
      this.DeleteButton.Location = new System.Drawing.Point(250, 31);
      this.DeleteButton.Name = "DeleteButton";
      this.DeleteButton.Size = new System.Drawing.Size(23, 23);
      this.DeleteButton.UseVisualStyleBackColor = true;
      this.DeleteButton.TabIndex = 3;
      this.DeleteButton.Click += new System.EventHandler(this.DeleteList);
      // 
      // SaveButton
      // 
      this.SaveButton.Cursor = System.Windows.Forms.Cursors.Hand;
      this.SaveButton.Image = global::iMS.Properties.Resources.Save;
      this.SaveButton.Location = new System.Drawing.Point(225, 31);
      this.SaveButton.Name = "SaveButton";
      this.SaveButton.Size = new System.Drawing.Size(23, 23);
      this.SaveButton.UseVisualStyleBackColor = true;
      this.SaveButton.TabIndex = 2;
      this.SaveButton.Click += new System.EventHandler(this.SaveList);
      // 
      // MembersOfRecipientsLabel
      // 
      this.MembersOfRecipientsLabel.Location = new System.Drawing.Point(5, 60);
      this.MembersOfRecipientsLabel.Name = "MembersOfRecipientsLabel";
      this.MembersOfRecipientsLabel.Size = new System.Drawing.Size(119, 13);
      this.MembersOfRecipientsLabel.Text = String.Concat(ResourceManager.Resource.GetString(Constants.WORDS.XXVII), Constants.SLUG.COLON);
      // 
      // MembersOfRecipientsValue
      // 
      this.MembersOfRecipientsValue.Location = new System.Drawing.Point(8, 80);
      this.MembersOfRecipientsValue.Multiline = true;
      this.MembersOfRecipientsValue.Name = "MembersOfRecipientsValue";
      this.MembersOfRecipientsValue.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
      this.MembersOfRecipientsValue.Size = new System.Drawing.Size(260, 151);
      this.MembersOfRecipientsValue.TabIndex = 1;
      // 
      // ListNameValue
      // 
      this.ListNameValue.Location = new System.Drawing.Point(64, 32);
      this.ListNameValue.Name = "ListNameValue";
      this.ListNameValue.Size = new System.Drawing.Size(146, 21);
      this.ListNameValue.TabIndex = 0;
      // 
      // ListName
      // 
      this.ListName.Location = new System.Drawing.Point(5, 35);
      this.ListName.Name = "ListName";
      this.ListName.Size = new System.Drawing.Size(57, 13);
      this.ListName.Text = String.Concat(ResourceManager.Resource.GetString(Constants.WORDS.XXVI), Constants.SLUG.COLON);
      // 
      // SavedListValue
      // 
      this.SavedListValue.FormattingEnabled = true;
      this.SavedListValue.Location = new System.Drawing.Point(64, 4);
      this.SavedListValue.Name = "SavedListValue";
      this.SavedListValue.DropDownStyle = ComboBoxStyle.DropDownList;
      this.SavedListValue.Size = new System.Drawing.Size(208, 21);
      this.SavedListValue.TabIndex = 4;
      // 
      // SavedListLabel
      // 
      this.SavedListLabel.Location = new System.Drawing.Point(5, 7);
      this.SavedListLabel.Name = "SavedListLabel";
      this.SavedListLabel.Size = new System.Drawing.Size(60, 13);
      this.SavedListLabel.Text = String.Concat(ResourceManager.Resource.GetString(Constants.WORDS.XXV), Constants.SLUG.COLON);
      //
      // erSaveList
      //
      this.erProvider.ContainerControl = this;
      // 
      // createCustomReceiverForm
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(292, 266);
      this.Controls.Add(this.CCRBox);
      this.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
      this.MaximizeBox = false;
      this.MinimizeBox = false;
      this.Name = "createCustomReceiverForm";
      this.ShowIcon = false;
      this.ShowInTaskbar = false;
      this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
      this.Text = ResourceManager.Resource.GetString(Constants.WORDS.VI);
      this.CCRBox.ResumeLayout(false);
      this.CCRPanel.ResumeLayout(false);
      this.CCRPanel.PerformLayout();
      ((System.ComponentModel.ISupportInitialize)(this.erProvider)).EndInit();
      this.ResumeLayout(false);

    }
    #endregion

    private System.Windows.Forms.GroupBox CCRBox;
    private System.Windows.Forms.Panel CCRPanel;
    private System.Windows.Forms.Label SavedListLabel;
    private System.Windows.Forms.ComboBox SavedListValue;
    private System.Windows.Forms.TextBox ListNameValue;
    private System.Windows.Forms.Label ListName;
    private System.Windows.Forms.TextBox MembersOfRecipientsValue;
    private System.Windows.Forms.Label MembersOfRecipientsLabel;
    private System.Windows.Forms.Button SaveButton;
    private System.Windows.Forms.Button DeleteButton;
    private System.Windows.Forms.ErrorProvider erProvider;
  }
}