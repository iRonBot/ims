﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using iMS.Classes;
using iMS.Resource;

namespace iMS
{
  public partial class createCustomReceiverForm : Form
  {
    public createCustomReceiverForm()
    {
      InitializeComponent();
      InitializeCRF(true);
    }

    private void InitializeCRF(bool isExtended)
    {
      DataTable dT = new DataTable();
      dT = Utils.GetCustomList(String.Concat(Account.cAccount,
                                                   Constants.SLUG.PIPE,
                                                   Account.cServer.Split(Constants.SLUG.DOT.ToCharArray())[Constants.NUMBERS.ZERO].ToString(),
                                                   Constants.SLUG.PIPE,
                                                   Account.cServer.Split(Constants.SLUG.DOT.ToCharArray())[Constants.NUMBERS.TWO].ToString()));
      if(dT.Rows.Count > Constants.NUMBERS.ZERO)
      {
        SavedListValue.DataSource = dT;
        if (SavedListValue.InvokeRequired)
        {
          SavedListValue.Invoke(new MethodInvoker(delegate
          {
            SavedListValue.DisplayMember = "PLAYER_DESC";
            SavedListValue.ValueMember = "ID_CODE";
          }));
        }
        else
        {
          SavedListValue.DisplayMember = "LIST_NAME";
          SavedListValue.ValueMember = "LIST_NAME";
        }
      }
      else
      {
        if(isExtended)
        {
          MessageBoxManager.OK = ResourceManager.Resource.GetString(Constants.WORDS.XVI);
          MessageBoxManager.Register();
          MessageBox.Show(ResourceManager.Resource.GetString(Constants.WORDS.XLVII),
                          ResourceManager.Resource.GetString(Constants.WORDS.XLVIII),
                          MessageBoxButtons.OK,
                          MessageBoxIcon.Asterisk,
                          MessageBoxDefaultButton.Button1,
                          ((Equals(ResourceManager.Resource.GetString("IsRightToLeft").ToLower(), Constants.BOOLEAN.YES)) ? MessageBoxOptions.RtlReading : MessageBoxOptions.DefaultDesktopOnly));
          MessageBoxManager.Unregister();
        }
      }
    }

    private void SaveList(object sender, EventArgs e)
    {
      if (String.IsNullOrEmpty(ListNameValue.Text.Trim()) ||
          String.IsNullOrEmpty(MembersOfRecipientsValue.Text.Trim()))
      {
        erProvider.BlinkStyle = ErrorBlinkStyle.NeverBlink;
        erProvider.SetError(ListNameValue, ResourceManager.Resource.GetString(Constants.WORDS.XLIX));
      }
      else
      {
        for (int i = Constants.NUMBERS.ZERO; i < MembersOfRecipientsValue.Lines.Length; i++)
        {
          Utils.INSERTCUSTOMLIST(String.Concat(Account.cAccount,
                                               Constants.SLUG.PIPE,
                                               Account.cServer.Split(Constants.SLUG.DOT.ToCharArray())[Constants.NUMBERS.ZERO].ToString(),
                                               Constants.SLUG.PIPE,
                                               Account.cServer.Split(Constants.SLUG.DOT.ToCharArray())[Constants.NUMBERS.TWO].ToString()),
                                               ListNameValue.Text,
                                               MembersOfRecipientsValue.Lines[i],
                                               Account.cServer);
        }
        InitializeCRF(false);
        erProvider.Clear();
        ListNameValue.Clear();
        MembersOfRecipientsValue.Clear();
      }
    }

    private void DeleteList(object sender, EventArgs e)
    {
      /* TODO */
    }
  }
}