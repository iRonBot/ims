﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using iMS.Classes;
using System.Text.RegularExpressions;

namespace iMS
{
  public partial class settingsForm : Form
  {
    public settingsForm()
    {
      InitializeComponent();
      InitializeForm();
    }
    private void InitializeForm()
    {
      String INFO = Utils.PROXY(Constants.PROXY.ALL);
      if(!String.IsNullOrEmpty(INFO))
      {
        DelayValue.Text = INFO.Split(Constants.SLUG.COLON.ToCharArray())[2].ToString();
        ProxyValue.Text = String.Concat(INFO.Split(Constants.SLUG.COLON.ToCharArray())[0].ToString(),
                                        Constants.SLUG.COLON,
                                        INFO.Split(Constants.SLUG.COLON.ToCharArray())[1].ToString());
        PageCountValue.Text = INFO.Split(Constants.SLUG.COLON.ToCharArray())[3].ToString();
      }
    }

    private void Save_Click(object sender, EventArgs e)
    {
      if(!Regex.IsMatch(ProxyValue.Text, Constants.REGEX.PROXY, RegexOptions.IgnoreCase))
      {
        /* قالب پروکسی وارد شده صحیح نمی‌باشد */
      }
      else
      {
        if(Equals(Utils.PROXY(Constants.PROXY.WRITE, String.Concat(ProxyValue.Text, Constants.SLUG.COLON, DelayValue.Text, Constants.SLUG.COLON, PageCountValue.Text)), Constants.UNIQUEPOINTER.D))
        {
          this.Hide();
        }
        else
        {
          /* در ذخیره‌ی اطلاعات مشکلی رخ داده است، تنظیمات ذخیره نشد */
        }
      }
    }
  }
}