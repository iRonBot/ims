/*
Navicat SQLite Data Transfer

Source Server         : iRonBot Message Sender
Source Server Version : 30623
Source Host           : localhost:0

Target Server Type    : SQLite
Target Server Version : 30623
File Encoding         : 65001

Date: 2013-03-05 13:00:23
*/

PRAGMA foreign_keys = OFF;

-- ----------------------------
-- Table structure for "main"."IMS_CUSTOM_LIST"
-- ----------------------------
DROP TABLE "main"."IMS_CUSTOM_LIST";
CREATE TABLE "IMS_CUSTOM_LIST" (
"ID_CODE"  INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL COLLATE NOCASE ,
"UNIQUE_CODE"  nvarchar(255) NOT NULL COLLATE NOCASE ,
"LIST_NAME"  nvarchar(255) NOT NULL COLLATE NOCASE ,
"PLAYER_DESC"  nvarchar(125) NOT NULL COLLATE NOCASE ,
"PLAYER_CODE"  nvarchar(6) COLLATE NOCASE ,
"RECORD_COUNTRY"  nvarchar(10) NOT NULL COLLATE NOCASE ,
"RECORD_SERVER"  nvarchar(20) NOT NULL COLLATE NOCASE ,
"IS_ACTIVE"  char(1) NOT NULL COLLATE NOCASE  DEFAULT A,
CONSTRAINT "FK_IMS_AL_CUSTOM_LIST" FOREIGN KEY ("UNIQUE_CODE") REFERENCES "IMS_ACCOUNT_LIST" ("UNIQUE_CODE") ON DELETE CASCADE ON UPDATE CASCADE
);

-- ----------------------------
-- Records of IMS_CUSTOM_LIST
-- ----------------------------
