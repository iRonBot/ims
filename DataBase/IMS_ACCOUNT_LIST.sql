/*
Navicat SQLite Data Transfer

Source Server         : iRonBot Message Sender
Source Server Version : 30706
Source Host           : :0

Target Server Type    : SQLite
Target Server Version : 30706
File Encoding         : 65001

Date: 2013-02-03 00:38:40
*/

PRAGMA foreign_keys = OFF;

-- ----------------------------
-- Table structure for "main"."IMS_ACCOUNT_LIST"
-- ----------------------------
DROP TABLE "main"."IMS_ACCOUNT_LIST";
CREATE TABLE "IMS_ACCOUNT_LIST" (
"ID_CODE"  INTEGER NOT NULL ON CONFLICT ROLLBACK COLLATE NOCASE PRIMARY KEY AUTOINCREMENT,
"UNIQUE_CODE"  nvarchar(225) NOT NULL ON CONFLICT ROLLBACK COLLATE NOCASE ,
"ACCOUNT_NAME"  nvarchar(75) NOT NULL ON CONFLICT ROLLBACK COLLATE NOCASE ,
"ACCOUNT_PASS"  nvarchar(50) NOT NULL ON CONFLICT ROLLBACK COLLATE NOCASE ,
"ACCOUNT_COUNTRY"  nvarchar(10) NOT NULL ON CONFLICT ROLLBACK COLLATE NOCASE ,
"ACCOUNT_SERVER"  nvarchar(20) NOT NULL ON CONFLICT ROLLBACK COLLATE NOCASE ,
"IS_ACTIVE"  char(1) NOT NULL ON CONFLICT ROLLBACK COLLATE NOCASE  DEFAULT A);

-- ----------------------------
-- Indexes structure for table IMS_ACCOUNT_LIST
-- ----------------------------
CREATE UNIQUE INDEX "main"."UNIQUE_CODE_INDEX"
ON "IMS_ACCOUNT_LIST" ("UNIQUE_CODE" ASC);