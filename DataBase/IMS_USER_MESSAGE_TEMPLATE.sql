/*
Navicat SQLite Data Transfer

Source Server         : iRonBot Message Sender
Source Server Version : 30706
Source Host           : :0

Target Server Type    : SQLite
Target Server Version : 30706
File Encoding         : 65001

Date: 2013-02-03 00:39:38
*/

PRAGMA foreign_keys = OFF;

-- ----------------------------
-- Table structure for "main"."IMS_USER_MESSAGE_TEMPLATE"
-- ----------------------------
DROP TABLE "main"."IMS_USER_MESSAGE_TEMPLATE";
CREATE TABLE "IMS_USER_MESSAGE_TEMPLATE" (
"ID_CODE" INTEGER NOT NULL ON CONFLICT ROLLBACK COLLATE NOCASE PRIMARY KEY AUTOINCREMENT,
"UNIQUE_CODE"  nvarchar(255) NOT NULL ON CONFLICT ROLLBACK COLLATE NOCASE ,
"MESSAGE_SUBJECT"  nvarchar(100) NOT NULL ON CONFLICT ROLLBACK COLLATE NOCASE ,
"MESSAGE_TEXT"  nvarchar(9804) NOT NULL ON CONFLICT ROLLBACK COLLATE NOCASE ,
"IS_ACTIVE"  char(1) NOT NULL ON CONFLICT ROLLBACK COLLATE NOCASE  DEFAULT A,
CONSTRAINT "FK_IMS_AL_USER_MESSAGE" FOREIGN KEY ("UNIQUE_CODE") REFERENCES "IMS_ACCOUNT_LIST" ("UNIQUE_CODE") ON DELETE CASCADE ON UPDATE CASCADE
);

-- ----------------------------
-- Records of IMS_USER_MESSAGE_TEMPLATE
-- ----------------------------
