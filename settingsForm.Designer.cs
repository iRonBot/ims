﻿using iMS.Resource;
using iMS.Classes;
namespace iMS
{
  partial class settingsForm
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(settingsForm));
      this.TimeBox = new System.Windows.Forms.GroupBox();
      this.TimeBoxPanel = new System.Windows.Forms.Panel();
      this.SecondLabel = new System.Windows.Forms.Label();
      this.DelayValue = new System.Windows.Forms.TextBox();
      this.TimeLabel = new System.Windows.Forms.Label();
      this.ProxyBox = new System.Windows.Forms.GroupBox();
      this.ProxyBoxPanel = new System.Windows.Forms.Panel();
      this.ProxyValue = new System.Windows.Forms.TextBox();
      this.ProxyLabel = new System.Windows.Forms.Label();
      this.Save = new System.Windows.Forms.Button();
      this.PageCountBox = new System.Windows.Forms.GroupBox();
      this.PageCountPanel = new System.Windows.Forms.Panel();
      this.PageCountLabel = new System.Windows.Forms.Label();
      this.PageCountValue = new System.Windows.Forms.TextBox();
      this.TimeBox.SuspendLayout();
      this.TimeBoxPanel.SuspendLayout();
      this.ProxyBox.SuspendLayout();
      this.ProxyBoxPanel.SuspendLayout();
      this.PageCountBox.SuspendLayout();
      this.PageCountPanel.SuspendLayout();
      this.SuspendLayout();
      // 
      // TimeBox
      // 
      this.TimeBox.Controls.Add(this.TimeBoxPanel);
      this.TimeBox.Location = new System.Drawing.Point(5, 5);
      this.TimeBox.Name = "TimeBox";
      this.TimeBox.Size = new System.Drawing.Size(284, 53);
      this.TimeBox.Text = ResourceManager.Resource.GetString(Constants.WORDS.XVII);
      // 
      // TimeBoxPanel
      // 
      this.TimeBoxPanel.BackColor = System.Drawing.SystemColors.Control;
      this.TimeBoxPanel.Controls.Add(this.SecondLabel);
      this.TimeBoxPanel.Controls.Add(this.DelayValue);
      this.TimeBoxPanel.Controls.Add(this.TimeLabel);
      this.TimeBoxPanel.Location = new System.Drawing.Point(3, 14);
      this.TimeBoxPanel.Name = "TimeBoxPanel";
      this.TimeBoxPanel.Size = new System.Drawing.Size(278, 33);
      // 
      // SecondLabel
      // 
      this.SecondLabel.Location = new System.Drawing.Point(240, 10);
      this.SecondLabel.Name = "SecondLabel";
      this.SecondLabel.Size = new System.Drawing.Size(41, 13);
      this.SecondLabel.Text = ResourceManager.Resource.GetString(Constants.WORDS.XIX);
      // 
      // DelayValue
      // 
      this.DelayValue.Location = new System.Drawing.Point(176, 6);
      this.DelayValue.MaxLength = 6;
      this.DelayValue.Name = "DelayValue";
      this.DelayValue.Size = new System.Drawing.Size(62, 21);
      this.DelayValue.Text = ResourceManager.Resource.GetString(Constants.WORDS.XXII);
      this.DelayValue.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      // 
      // TimeLabel
      // 
      this.TimeLabel.Location = new System.Drawing.Point(3, 10);
      this.TimeLabel.Name = "TimeLabel";
      this.TimeLabel.Size = new System.Drawing.Size(177, 13);
      this.TimeLabel.Text = ResourceManager.Resource.GetString(Constants.WORDS.XVIII);
      // 
      // ProxyBox
      // 
      this.ProxyBox.Controls.Add(this.ProxyBoxPanel);
      this.ProxyBox.Location = new System.Drawing.Point(5, 64);
      this.ProxyBox.Name = "ProxyBox";
      this.ProxyBox.Size = new System.Drawing.Size(284, 78);
      this.ProxyBox.Text = ResourceManager.Resource.GetString(Constants.WORDS.XX);
      // 
      // ProxyBoxPanel
      // 
      this.ProxyBoxPanel.BackColor = System.Drawing.SystemColors.Control;
      this.ProxyBoxPanel.Controls.Add(this.ProxyValue);
      this.ProxyBoxPanel.Controls.Add(this.ProxyLabel);
      this.ProxyBoxPanel.Location = new System.Drawing.Point(3, 14);
      this.ProxyBoxPanel.Name = "ProxyBoxPanel";
      this.ProxyBoxPanel.Size = new System.Drawing.Size(278, 58);
      // 
      // ProxyValue
      // 
      this.ProxyValue.Location = new System.Drawing.Point(6, 31);
      this.ProxyValue.MaxLength = 21;
      this.ProxyValue.Name = "ProxyValue";
      this.ProxyValue.Size = new System.Drawing.Size(155, 21);
      this.ProxyValue.Text = ResourceManager.Resource.GetString(Constants.WORDS.XXIII);
      // 
      // ProxyLabel
      // 
      this.ProxyLabel.Location = new System.Drawing.Point(3, 10);
      this.ProxyLabel.Name = "ProxyLabel";
      this.ProxyLabel.Size = new System.Drawing.Size(278, 13);
      this.ProxyLabel.Text = ResourceManager.Resource.GetString(Constants.WORDS.XXI);
      // 
      // Save
      // 
      this.Save.Cursor = System.Windows.Forms.Cursors.Hand;
      this.Save.Location = new System.Drawing.Point(110, 210);
      this.Save.Name = "Save";
      this.Save.Size = new System.Drawing.Size(75, 23);
      this.Save.Text = ResourceManager.Resource.GetString(Constants.WORDS.XV);
      this.Save.UseVisualStyleBackColor = true;
      this.Save.Click += new System.EventHandler(this.Save_Click);
      // 
      // PageCountBox
      // 
      this.PageCountBox.Controls.Add(this.PageCountPanel);
      this.PageCountBox.Location = new System.Drawing.Point(5, 148);
      this.PageCountBox.Name = "PageCountBox";
      this.PageCountBox.Size = new System.Drawing.Size(284, 54);
      this.PageCountBox.Text = ResourceManager.Resource.GetString(Constants.WORDS.XLI);
      // 
      // PageCountPanel
      // 
      this.PageCountPanel.Controls.Add(this.PageCountValue);
      this.PageCountPanel.Controls.Add(this.PageCountLabel);
      this.PageCountPanel.Location = new System.Drawing.Point(3, 14);
      this.PageCountPanel.Name = "PageCountPanel";
      this.PageCountPanel.Size = new System.Drawing.Size(278, 34);
      // 
      // PageCountLabel
      // 
      this.PageCountLabel.AutoSize = true;
      this.PageCountLabel.Location = new System.Drawing.Point(3, 10);
      this.PageCountLabel.Name = "PageCountLabel";
      this.PageCountLabel.Size = new System.Drawing.Size(179, 13);
      this.PageCountLabel.Text = ResourceManager.Resource.GetString(Constants.WORDS.XLII);
      // 
      // PageCountValue
      // 
      this.PageCountValue.Location = new System.Drawing.Point(183, 6);
      this.PageCountValue.Name = "PageCountValue";
      this.PageCountValue.Size = new System.Drawing.Size(88, 21);
      this.PageCountValue.Text = ResourceManager.Resource.GetString(Constants.WORDS.XXII);
      this.PageCountValue.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
      // 
      // settingsForm
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(294, 241);
      this.Controls.Add(this.PageCountBox);
      this.Controls.Add(this.Save);
      this.Controls.Add(this.ProxyBox);
      this.Controls.Add(this.TimeBox);
      this.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
      this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
      this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
      this.MaximizeBox = false;
      this.MinimizeBox = false;
      this.Name = "settingsForm";
      this.RightToLeftLayout = true;
      this.ShowIcon = false;
      this.ShowInTaskbar = false;
      this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
      this.Text = ResourceManager.Resource.GetString(Constants.WORDS.V);
      this.TopMost = true;
      this.TimeBox.ResumeLayout(false);
      this.TimeBoxPanel.ResumeLayout(false);
      this.TimeBoxPanel.PerformLayout();
      this.ProxyBox.ResumeLayout(false);
      this.ProxyBoxPanel.ResumeLayout(false);
      this.ProxyBoxPanel.PerformLayout();
      this.PageCountBox.ResumeLayout(false);
      this.PageCountPanel.ResumeLayout(false);
      this.PageCountPanel.PerformLayout();
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.GroupBox TimeBox;
    private System.Windows.Forms.Panel TimeBoxPanel;
    private System.Windows.Forms.GroupBox ProxyBox;
    private System.Windows.Forms.Panel ProxyBoxPanel;
    private System.Windows.Forms.Label TimeLabel;
    private System.Windows.Forms.Label SecondLabel;
    private System.Windows.Forms.TextBox DelayValue;
    private System.Windows.Forms.Label ProxyLabel;
    private System.Windows.Forms.TextBox ProxyValue;
    private System.Windows.Forms.Button Save;
    private System.Windows.Forms.GroupBox PageCountBox;
    private System.Windows.Forms.Panel PageCountPanel;
    private System.Windows.Forms.TextBox PageCountValue;
    private System.Windows.Forms.Label PageCountLabel;

  }
}