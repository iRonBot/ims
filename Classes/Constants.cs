﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using iMS.Resource;

namespace iMS.Classes
{
  class Constants
  {
    public class KEYS
    {
      public static String i10 = "i10";
      public static String i20 = "i20";
      public static String i50 = "i50";
      public static String iAL = "iAL";
    }
    public class WYSIWYG
    {
      public static String BOLD = "[b][/b]";
      public static String ITALIC = "[i][/i]";
      public static String UNDERLINE = "[u][/u]";
      public static String QUOTE = "[quote][/quote]";
      public static String COLOR = String.Concat("[color=", QUERY.FIRST, "][/color]");
    }
    public class NUMBERS
    {
      public static int MINUS_ONE = -1;
      public static int ZERO = 0;
      public static int ONE = 1;
      public static int TWO = 2;
      public static int THREE = 3;
      public static int FOUR = 4;
      public static int FIVE = 5;
      public static int SIX = 6;
      public static int SEVEN = 7;
      public static int EIGHT = 8;
      public static int NINE = 9;
      public static int TEN = 10;
      public static int TWENTY = 20;
      public static int FIFTY = 50;
    }
    public class BOOLEAN
    {
      public static String YES = "yes";
      public static String NO = "no";
    }
    public class DATABASE
    {
      public static String ADDRESS = "iMS_DB";
      public static String CLASS = @"Data Source={0};Version=3;Legacy Format=True;UTF8Encoding=True;";
    }
    public class COLORS
    {
      public static Color ERROR = System.Drawing.ColorTranslator.FromHtml("#FF3333");
      public static Color ATTENTION = System.Drawing.ColorTranslator.FromHtml("#CC66FF");
      public static Color SUCCESS = System.Drawing.ColorTranslator.FromHtml("#33CC33");
      public static Color NONE = Color.Transparent;
    }
    public class UNIQUEPOINTER
    {
      public static String B = "BEGIN";
      public static String E = "END";
      public static String D = "DONE";
    }
    public class CONTENTTYPE
    {
      public static String POST = "application/x-www-form-urlencoded";
    }
    public class USERAGENT
    {
      public static String FIREFOX = "Mozilla/5.0 (Windows NT 5.1; rv:10.0.2) Gecko/20100101 Firefox/10.0.2";
    }
    public class PROXY
    {
      public static String FILE_NAME = "IMS_INITIALIZATION";
      public static String FILE_EXTENSION = ".ini";
      public static String PROXY_NODE = "PROXY.CONFIGURE";
      public static String DELAY_NODE = "DELAY.CONFIGURE";
      public static String TIME_NAME = "TIME";
      public static String IP_NAME = "IP";
      public static String PORT_NAME = "PORT";
      public static String PAGE_NODE = "PAGE.CONFIGURE";
      public static String PAGE_NAME = "PAGE";
      public static int ALL   = 0;
      public static int READ  = 1;
      public static int WRITE = 2;
      public static String FULL = "127.0.0.1:80:30:30";
      public static String MINI = "127.0.0.1:80";
    }
    public class WORDS
    {
      public static String I       = "_1st";
      public static String II      = "_2nd";
      public static String III     = "_3rd";
      public static String IV      = "_4th";
      public static String V       = "_5th";
      public static String VI      = "_6th";
      public static String VII     = "_7th";
      public static String VIII    = "_8th";
      public static String IX      = "_9th";
      public static String X       = "_10th";
      public static String XI      = "_11th";
      public static String XII     = "_12th";
      public static String XIII    = "_13th";
      public static String XIV     = "_14th";
      public static String XV      = "_15th";
      public static String XVI     = "_16th";
      public static String XVII    = "_17th";
      public static String XVIII   = "_18th";
      public static String XIX     = "_19th";
      public static String XX      = "_20th";
      public static String XXI     = "_21th";
      public static String XXII    = "_22th";
      public static String XXIII   = "_23th";
      public static String XXIV    = "_24th";
      public static String XXV     = "_25th";
      public static String XXVI    = "_26th";
      public static String XXVII   = "_27th";
      public static String XXVIII  = "_28th";
      public static String XXIX    = "_29th";
      public static String XXX     = "_30th";
      public static String XXXI    = "_31th";
      public static String XXXII   = "_32th";
      public static String XXXIII  = "_33th";
      public static String XXXIV   = "_34th";
      public static String XXXV    = "_35th";
      public static String XXXVI   = "_36th";
      public static String XXXVII  = "_37th";
      public static String XXXVIII = "_38th";
      public static String XXXIX   = "_39th";
      public static String XL      = "_40th";
      public static String XLI     = "_41th";
      public static String XLII    = "_42th";
      public static String XLIII   = "_43th";
      public static String XLIV    = "_44th";
      public static String XLV     = "_45th";
      public static String XLVI    = "_46th";
      public static String XLVII   = "_47th";
      public static String XLVIII  = "_48th";
      public static String XLIX    = "_49th";
      public static String L       = "_50th";
      public static String LI      = "_51th";
      public static String LII     = "_52th";
      public static String LIII    = "_53th";
      public static String LIV     = "_54th";
      public static String LV      = "_55th";
      public static String LVI     = "_56th";
      public static String LVII    = "_57th";
      public static String LVIII   = "_58th";
      public static String LIX     = "_59th";
      public static String LX      = "_60th";

    }
    public class PARENTHESIS
    {
      public static String LEFT = ")";
      public static String RIGHT = "(";
    }
    public class QUERY
    {
      public static String FIRST = "{0}";
      public static String SECOND = "{1}";
      public static String THIRD = "{2}";
      public static String COLUMNS = " {0},";
      public static String VALUE = " '{0}',";
      public static String INSERT = "INSERT INTO ";
      public static String VALUES = " VALUES";
      public static String UPDATE = "UPDATE ";
      public static String SET = " SET ";
      public static String WHERE = " WHERE ";
      public static String DELETE = "DELETE FROM ";
    }
    public class SLUG
    {
      public static String EMPTY = "";
      public static String SPACE = " ";
      public static String DOT = ".";
      public static String SLASH = "/";
      public static String COLON = ":";
      public static String SEMICOLON = ";";
      public static String PIPE = "|";
      public static String UNDERLINE = "_";
      public static String FIRST = " {0}";
      public static String SECOND = " {1}";
      public static String[] BASIC_STRUCTURE = { "yes",
                                                 "travian.",
                                                 "en-US",
                                                 "",
                                                 "",
                                                 "",
                                                 "",
                                                 "",
                                                 ""};
    }
    public class REGEX
    {
      public static String TRAVIAN = "[a-z0-9]*.travian.[a-z.]*";
      public static String SERVER = "[a-z0-9]*.";
      public static String PROXY = "^\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}:\\d{1,5}$";
      public static String LASTPAGE = "<img alt=\"([\\s\\S]*)\" src=\"img/x.gif\" /></a> <a href=\"statistiken.php\\?id=0&amp;idSub=0&amp;page=";
      public static String UID = "<a href=\"spieler.php\\?uid=[0-9]{0,6}\">";
    }
    public class METHOD
    {
      public static String GET = "GET";
      public static String POST = "POST";
    }
    public class PROTOCOL
    {
      public static String HTTP = "http://";
      public static String HTTPS = "https://";
    }
    public class PAGES
    {
      public static String LOGIN = "login.php";
    }
    public class ERRORS
    {
      public static String CAN_NOT_FETCH = "errCanNotFetch";
      public static String NOT_EXISTS = "errNotExists";
      public static String IS_EXISTS = "errIsExists";
      public static String RESULT_IS_NULL = "errResultIsNull";
      public static String EXCEPTION_OCCURRED = "errExceptionOccurred";
      public static String CAN_NOT_LOGIN = "errCanNotLogin";
      public static String SQLITE_EXCEPTION = "errSQLiteException";
      public static String SQLITE_EXCEPTION_CANNOT_INSERT = "errCanNotInsertUniqueKey";
    }
    public class EXCEPTION
    {
      public static String UNIQUE_KEY = "Abort due to constraint violation\r\ncolumn UNIQUE_CODE is not unique";
      public static String INDEX_OUTSIDE = "Index was outside the bounds of the array.";
      public static String DATABASE_NOT_FOUND = "SQLite error\r\nno such table: IMS_ACCOUNT_LIST";
    }
    public class CULTURE
    {
      public static String CURRENT = System.Globalization.CultureInfo.CurrentCulture.Name;
    }
  }
}