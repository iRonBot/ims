﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace iMS.Classes
{
  class Inquiries
  {
    public static String GET_ACCOUNT_LIST = String.Concat("SELECT ACCOUNT_SERVER ||'",
                                                          Constants.SLUG.PIPE,
                                                          "'|| ACCOUNT_NAME ||'",
                                                          Constants.SLUG.PIPE,
                                                          "'|| ACCOUNT_COUNTRY AS MAGIC_ID, ACCOUNT_PASS FROM ",
                                                          Tables.Define.ACCOUNT_LIST);

    public static String GET_USER_LIST = String.Concat("SELECT ID_CODE, PLAYER_DESC FROM ",
                                                       Tables.Define.SERVER_USER_LIST,
                                                       " WHERE UNIQUE_CODE = '",
                                                       Constants.QUERY.FIRST,
                                                       "' AND IS_ACTIVE = 'A'");

    public static String GET_CUSTOM_LIST = String.Concat("SELECT DISTINCT LIST_NAME FROM ",
                                                         Tables.Define.CUSTOM_LIST,
                                                         " WHERE UNIQUE_CODE = '",
                                                         Constants.QUERY.FIRST,
                                                         "' AND IS_ACTIVE = 'A' GROUP BY LIST_NAME ORDER BY ID_CODE DESC");

    public static String GET_TEMPLATE_LIST = String.Concat("SELECT ID_CODE, MESSAGE_SUBJECT FROM ",
                                                           Tables.Define.USER_MESSAGE_TEMPLATE,
                                                           " WHERE UNIQUE_CODE = '",
                                                           Constants.QUERY.FIRST,
                                                           "' AND IS_ACTIVE = 'A'");

    public static String GET_MESSAGE_TEXT_BY_ID = String.Concat("SELECT MESSAGE_TEXT FROM ",
                                                                Tables.Define.USER_MESSAGE_TEMPLATE,
                                                                " WHERE ID_CODE = '",
                                                                Constants.QUERY.FIRST,
                                                                "' AND IS_ACTIVE = 'A'");

    public static String GET_PLAYERS_LIST_BY_NAME = String.Concat("SELECT PLAYER_DESC FROM ",
                                                                  Tables.Define.CUSTOM_LIST,
                                                                  " WHERE LIST_NAME = '",
                                                                  Constants.QUERY.FIRST,
                                                                  "' AND UNIQUE_CODE = '",
                                                                  Constants.QUERY.SECOND,
                                                                  "' AND IS_ACTIVE = 'A'");
  }
}
namespace iMS.Tables
{
  class Define
  {
    public static String ACCOUNT_LIST = "IMS_ACCOUNT_LIST";
    public static String CUSTOM_LIST = "IMS_CUSTOM_LIST";
    public static String SERVER_USER_LIST = "IMS_USER_LIST_SERVER";
    public static String USER_MESSAGE_TEMPLATE = "IMS_USER_MESSAGE_TEMPLATE";
  }
}