﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;
using System.Net;
using iMS.Initialization;
using iMS.Resource;
using System.IO;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using System.Data;
using iMS.Tables;

namespace iMS.Classes
{
  class Utils
  {
    public static CookieContainer SID = new CookieContainer();

    public static String RemoveSpecialCharacters(String Data)
    {
      return ((Data.Replace("\n", "")).Replace("\t", "")).Replace("\r", "");
    }
    public static System.Windows.Forms.RightToLeft Direction(String Current)
    {
      return ((Equals(Current.ToLower(), Constants.SLUG.BASIC_STRUCTURE[0])) ? System.Windows.Forms.RightToLeft.Yes : System.Windows.Forms.RightToLeft.No);
    }
    public static void MessageDrawer(System.Windows.Forms.Label TARGET, System.Drawing.Color COLOR, String TEXT, bool isSplit = false, String dParam = null) { if (isSplit) { TARGET.ForeColor = COLOR; if (TARGET.InvokeRequired) { TARGET.Invoke(new MethodInvoker(delegate { TARGET.Text = String.Format(TEXT, dParam); })); } else { TARGET.Text = String.Format(TEXT, dParam); } } else { TARGET.ForeColor = COLOR; if (TARGET.InvokeRequired) { TARGET.Invoke(new MethodInvoker(delegate { TARGET.Text = TEXT; })); } else { TARGET.Text = TEXT; } } }
    public static bool isValidURL(String URL) { return isValidURL(URL, false); }
    public static bool isValidURL(String URL, bool hProxy = false)
    {
      try
      {
        WebProxy wProxy = new WebProxy();
        HttpWebRequest REQ = (HttpWebRequest)WebRequest.Create(String.Concat(URL, Constants.PAGES.LOGIN));
        if (hProxy)
        {
          wProxy = new WebProxy(Utils.URL2URI(String.Concat(new InitializationFile(String.Concat(Constants.PROXY.FILE_NAME, Constants.PROXY.FILE_EXTENSION)).ReadValue(Constants.PROXY.PROXY_NODE, Constants.PROXY.IP_NAME),
                                                                                                 Constants.SLUG.COLON,
                                                                                                 new InitializationFile(String.Concat(Constants.PROXY.FILE_NAME, Constants.PROXY.FILE_EXTENSION)).ReadValue(Constants.PROXY.PROXY_NODE, Constants.PROXY.PORT_NAME))));
          wProxy.BypassProxyOnLocal = true;
        }
        REQ.Method = Constants.METHOD.POST;
        REQ.ContentLength = 0;
        REQ.KeepAlive = true;
        if (hProxy) { REQ.Proxy = wProxy; }
        HttpWebResponse RES = (HttpWebResponse)REQ.GetResponse();
        if (RES.Headers == null) { throw new Exception(Constants.ERRORS.RESULT_IS_NULL); }
        RES.Close();
        return true;
      }
      catch
      // catch (WebException e)
      {
        // e.ToString();
        return false;
      }
    }
    public static bool isEmpty(String Data)
    {
      if(String.IsNullOrEmpty(Data.Trim()) ||
         Equals(Data, ResourceManager.Resource.GetString("Username")) ||
         Equals(Data, ResourceManager.Resource.GetString("Password")) ||
         Equals(Data, ResourceManager.Resource.GetString("Server").ToLower()))
      {
        return true;
      }
      return false;
    }
    public static String pValue(String Data, String Begin, String End)
    {
      return pValue(Data, Begin, End, 1);
    }
    public static String pValue(String Data, String Begin, String End, int Pointer = 1)
    {
      try
      {
        int[] Counter = { 0, 0, 0 };

        for (int i = 0; i < Data.Length; i++)
        {
          if (Begin == Data.Substring(i, Begin.Length))
          {
            Counter[2] = i + Begin.Length;
            for (int j = Counter[2]; j < Data.Length; j++)
            {
              if (End == Data.Substring(j, End.Length))
              {
                Counter[0]++;
                Counter[1] = j;
                if (Counter[0] != Pointer) { break; }
                return Data.Substring(Counter[2], Counter[1] - Counter[2]);
              }
            }
          }
        }
        return Constants.ERRORS.CAN_NOT_FETCH;
      }
      catch
      // catch (ArgumentException e)
      {
        // e.ToString();
        return Constants.ERRORS.CAN_NOT_FETCH;
      }
    }
    public static String isExists(String Data, String Value) { return isExists(Data, Value, false, ""); }
    public static String isExists(String Data, String Value, bool Find, String Pointer)
    {
      String RET = Constants.ERRORS.CAN_NOT_FETCH;
      try
      {
        if (Find)
        {
          /* باید دنبال اطلاعات بگردم */
          RET = Data.Substring(Data.IndexOf(Value),
                               Data.Substring(Data.IndexOf(Value)).IndexOf(Pointer)).Replace(Value, "");
        }
        else
        {
          /* فقط باید بگم وجود دارد یا خیر */
          if (Data.IndexOf(Value) == -1)
          {
            RET = Constants.ERRORS.NOT_EXISTS;
          }
          else
          {
            RET = Constants.ERRORS.IS_EXISTS;
          }
        }
        return RET;
      }
      catch
      // catch (ArgumentException e)
      {
        // e.ToString();
        return RET;
      }
    }
    public static String _POST(String TARGETURL, String POSTDATA, String REFERER = "", bool USECOOKIE = false, CookieContainer COOKIE = null) { return _POSTLOGIN(TARGETURL, POSTDATA, REFERER, USECOOKIE, COOKIE, false); }
    public static String _POST(String TARGETURL, String POSTDATA, String REFERER = "", bool USECOOKIE = false, CookieContainer COOKIE = null, bool hProxy = false) { return _POSTLOGIN(TARGETURL, POSTDATA, REFERER, USECOOKIE, COOKIE, hProxy); }
    public static String _POSTLOGIN(String TARGETURL, String POSTDATA)
    {
      return _POSTLOGIN(TARGETURL, POSTDATA, "", false, null, false);
    }
    public static String _POSTLOGIN(String TARGETURL, String POSTDATA, bool hProxy = false)
    {
      return _POSTLOGIN(TARGETURL, POSTDATA, "", false, null, hProxy);
    }
    public static String _POSTLOGIN(String TARGETURL, String POSTDATA, String REFERER = "", bool USECOOKIE = false, CookieContainer COOKIE = null, bool hProxy = false)
    {
      try
      {
        WebProxy wProxy = new WebProxy();
        HttpWebRequest REQ = (HttpWebRequest)WebRequest.Create(TARGETURL);
        if (hProxy)
        {
          wProxy = new WebProxy(Utils.URL2URI(String.Concat(new InitializationFile(String.Concat(Constants.PROXY.FILE_NAME, Constants.PROXY.FILE_EXTENSION)).ReadValue(Constants.PROXY.PROXY_NODE, Constants.PROXY.IP_NAME),
                                                                                                 Constants.SLUG.COLON,
                                                                                                 new InitializationFile(String.Concat(Constants.PROXY.FILE_NAME, Constants.PROXY.FILE_EXTENSION)).ReadValue(Constants.PROXY.PROXY_NODE, Constants.PROXY.PORT_NAME))));
          wProxy.BypassProxyOnLocal = true;
        }
        REQ.AllowAutoRedirect = true;
        REQ.UserAgent = Constants.USERAGENT.FIREFOX;
        REQ.KeepAlive = true;
        REQ.Method = Constants.METHOD.POST;
        REQ.Referer = REFERER;
        REQ.ServicePoint.Expect100Continue = false;
        REQ.ContentType = Constants.CONTENTTYPE.POST;
        REQ.ProtocolVersion = HttpVersion.Version10;
        REQ.CookieContainer = SID;
        // if (USECOOKIE) { REQ.CookieContainer = SID; }

        byte[] Bytes = Encoding.ASCII.GetBytes(POSTDATA);

        REQ.ContentLength = Bytes.Length;

        if (hProxy) { REQ.Proxy = wProxy; }

        Stream STM = REQ.GetRequestStream();

        STM.Write(Bytes, 0, Bytes.Length);

        STM.Close();

        HttpWebResponse RES = (HttpWebResponse)REQ.GetResponse();

        RES.Close();

        String VALUE = RES.Headers.ToString();
        return String.Concat(_gHTML(TARGETURL, TARGETURL),
                             Constants.UNIQUEPOINTER.B,
                             VALUE,
                             Constants.UNIQUEPOINTER.E);
      }
      catch (Exception e)
      {
        e.ToString();
        return Constants.ERRORS.CAN_NOT_LOGIN;
      }
    }
    public static String _gHTML(String URL, String REFERER, bool hProxy = false)
    {
      WebProxy wProxy = new WebProxy();
      HttpWebRequest REQ = (HttpWebRequest)WebRequest.Create(URL);
      if (hProxy)
      {
        wProxy = new WebProxy(Utils.URL2URI(String.Concat(new InitializationFile(String.Concat(Constants.PROXY.FILE_NAME, Constants.PROXY.FILE_EXTENSION)).ReadValue(Constants.PROXY.PROXY_NODE, Constants.PROXY.IP_NAME),
                                                                                               Constants.SLUG.COLON,
                                                                                               new InitializationFile(String.Concat(Constants.PROXY.FILE_NAME, Constants.PROXY.FILE_EXTENSION)).ReadValue(Constants.PROXY.PROXY_NODE, Constants.PROXY.PORT_NAME))));
        wProxy.BypassProxyOnLocal = true;
      }
      REQ.Referer = REFERER;
      REQ.AllowAutoRedirect = true;
      REQ.ServicePoint.Expect100Continue = false;
      REQ.Timeout = 10000;
      REQ.CookieContainer = SID;
      REQ.Method = Constants.METHOD.GET;
      REQ.UserAgent = Constants.USERAGENT.FIREFOX;
      if (hProxy) { REQ.Proxy = wProxy; }
      HttpWebResponse RES = (HttpWebResponse)REQ.GetResponse();
      StreamReader SR = new StreamReader(RES.GetResponseStream());
      String rHTML = SR.ReadToEnd();
      SR.Close();
      RES.Close();
      return rHTML;
    }
    public static String _GETINFO(String URL, String REFERER) { return _GETINFO(URL, REFERER, null, false, false); }
    public static String _GETINFO(String URL, String REFERER, bool hProxy = false) { return _GETINFO(URL, REFERER, null, false, hProxy); }
    public static String _GETINFO(String URL, String REFERER, CookieContainer COOKIE, bool USECOOKIE) { return _GETINFO(URL, REFERER, COOKIE, USECOOKIE, false); }
    public static String _GETINFO(String URL, String REFERER, CookieContainer COOKIE, bool USECOOKIE, bool hProxy = false)
    {
      // ServicePointManager.Expect100Continue = false;
      /*
      try
      {
        WebProxy wProxy = new WebProxy();
        HttpWebRequest REQ = (HttpWebRequest)WebRequest.Create(URL);
        if (hProxy)
        {
          wProxy = new WebProxy(Utils.URL2URI(String.Concat(new InitializationFile(String.Concat(Constants.PROXY.FILE_NAME, Constants.PROXY.FILE_EXTENSION)).ReadValue(Constants.PROXY.PROXY_NODE, Constants.PROXY.IP_NAME),
                                                                                                 Constants.SLUG.COLON,
                                                                                                 new InitializationFile(String.Concat(Constants.PROXY.FILE_NAME, Constants.PROXY.FILE_EXTENSION)).ReadValue(Constants.PROXY.PROXY_NODE, Constants.PROXY.PORT_NAME))));
          wProxy.BypassProxyOnLocal = true;
        }
        if (USECOOKIE) { REQ.CookieContainer = COOKIE; }
        REQ.ServicePoint.Expect100Continue = false;
        REQ.UserAgent = Constants.USERAGENT.FIREFOX;
        REQ.KeepAlive = true;
        REQ.Method = Constants.METHOD.GET;
        if (hProxy) { REQ.Proxy = wProxy; }
        HttpWebResponse RES = (HttpWebResponse)REQ.GetResponse();
        if (USECOOKIE)
        {
          String KEY = new StreamReader(RES.GetResponseStream()).ReadToEnd();
          String VALUE = RES.Headers.ToString();
          return String.Concat(KEY, Constants.UNIQUEPOINTER.B, VALUE, Constants.UNIQUEPOINTER.E);
        }
        else
        {
          StreamReader RED = new StreamReader(RES.GetResponseStream());
          return RED.ReadToEnd();
        }
      }
      catch
      {
        return Constants.ERRORS.EXCEPTION_OCCURRED;
      }
      */
      try
      {
        WebProxy wProxy = new WebProxy();
        HttpWebRequest REQ = (HttpWebRequest)WebRequest.Create(URL);
        if (hProxy)
        {
          wProxy = new WebProxy(Utils.URL2URI(String.Concat(new InitializationFile(String.Concat(Constants.PROXY.FILE_NAME, Constants.PROXY.FILE_EXTENSION)).ReadValue(Constants.PROXY.PROXY_NODE, Constants.PROXY.IP_NAME),
                                                                                                 Constants.SLUG.COLON,
                                                                                                 new InitializationFile(String.Concat(Constants.PROXY.FILE_NAME, Constants.PROXY.FILE_EXTENSION)).ReadValue(Constants.PROXY.PROXY_NODE, Constants.PROXY.PORT_NAME))));
          wProxy.BypassProxyOnLocal = true;
        }
        REQ.Referer = REFERER;
        REQ.AllowAutoRedirect = true;
        REQ.ServicePoint.Expect100Continue = false;
        REQ.Timeout = 10000;
        REQ.CookieContainer = SID;
        REQ.Method = Constants.METHOD.GET;
        REQ.UserAgent = Constants.USERAGENT.FIREFOX;
        if (hProxy) { REQ.Proxy = wProxy; }
        HttpWebResponse RES = (HttpWebResponse)REQ.GetResponse();
        /*
        StreamReader SR = new StreamReader(RES.GetResponseStream());
        String rHTML = SR.ReadToEnd();
        SR.Close();
        RES.Close();
        return rHTML;
        */
        if (USECOOKIE)
        {
          String KEY = new StreamReader(RES.GetResponseStream()).ReadToEnd();
          String VALUE = RES.Headers.ToString();
          return String.Concat(KEY, Constants.UNIQUEPOINTER.B, VALUE, Constants.UNIQUEPOINTER.E);
        }
        else
        {
          StreamReader RED = new StreamReader(RES.GetResponseStream());
          return RED.ReadToEnd();
        }
      }
      catch
      {
        return Constants.ERRORS.EXCEPTION_OCCURRED;
      }
    }
    public static Uri URL2URI(String URL) { return URL2URI(URL, false); }
    public static Uri URL2URI(String URL, bool IsSplitted) { if (IsSplitted) { return new Uri(String.Concat(Constants.PROTOCOL.HTTP, URL.Replace(Regex.Match(URL, Constants.REGEX.SERVER).ToString(), String.Concat(Regex.Match(URL, Constants.REGEX.SERVER).ToString(), Constants.SLUG.BASIC_STRUCTURE[1].ToString())))); } else { return new Uri(String.Concat(Constants.PROTOCOL.HTTP, URL)); } }
    public static bool FileExists()
    {
      if (!String.IsNullOrEmpty(new InitializationFile(String.Concat(Constants.PROXY.FILE_NAME, Constants.PROXY.FILE_EXTENSION)).ReadValue(Constants.PROXY.PROXY_NODE, Constants.PROXY.IP_NAME)) ||
          !String.IsNullOrEmpty(new InitializationFile(String.Concat(Constants.PROXY.FILE_NAME, Constants.PROXY.FILE_EXTENSION)).ReadValue(Constants.PROXY.PROXY_NODE, Constants.PROXY.PORT_NAME)) ||
          !String.IsNullOrEmpty(new InitializationFile(String.Concat(Constants.PROXY.FILE_NAME, Constants.PROXY.FILE_EXTENSION)).ReadValue(Constants.PROXY.DELAY_NODE, Constants.PROXY.TIME_NAME)) ||
          !String.IsNullOrEmpty(new InitializationFile(String.Concat(Constants.PROXY.FILE_NAME, Constants.PROXY.FILE_EXTENSION)).ReadValue(Constants.PROXY.PAGE_NODE, Constants.PROXY.PAGE_NAME)))
      {
        return true;
      }
      else { return false; }
    }
    public static String PROXY(int FLAG) { return PROXY(FLAG, null); }
    public static String PROXY(int FLAG, String INFO)
    {
      if (Equals(FLAG, Constants.PROXY.ALL))
      {
        if (!String.IsNullOrEmpty(new InitializationFile(String.Concat(Constants.PROXY.FILE_NAME, Constants.PROXY.FILE_EXTENSION)).ReadValue(Constants.PROXY.PROXY_NODE, Constants.PROXY.IP_NAME)) ||
            !String.IsNullOrEmpty(new InitializationFile(String.Concat(Constants.PROXY.FILE_NAME, Constants.PROXY.FILE_EXTENSION)).ReadValue(Constants.PROXY.PROXY_NODE, Constants.PROXY.PORT_NAME)) ||
            !String.IsNullOrEmpty(new InitializationFile(String.Concat(Constants.PROXY.FILE_NAME, Constants.PROXY.FILE_EXTENSION)).ReadValue(Constants.PROXY.DELAY_NODE, Constants.PROXY.TIME_NAME)) ||
            !String.IsNullOrEmpty(new InitializationFile(String.Concat(Constants.PROXY.FILE_NAME, Constants.PROXY.FILE_EXTENSION)).ReadValue(Constants.PROXY.PAGE_NODE, Constants.PROXY.PAGE_NAME))
          )
        {
          return String.Concat(new InitializationFile(String.Concat(Constants.PROXY.FILE_NAME, Constants.PROXY.FILE_EXTENSION)).ReadValue(Constants.PROXY.PROXY_NODE, Constants.PROXY.IP_NAME),
                               Constants.SLUG.COLON,
                               new InitializationFile(String.Concat(Constants.PROXY.FILE_NAME, Constants.PROXY.FILE_EXTENSION)).ReadValue(Constants.PROXY.PROXY_NODE, Constants.PROXY.PORT_NAME),
                               Constants.SLUG.COLON,
                               new InitializationFile(String.Concat(Constants.PROXY.FILE_NAME, Constants.PROXY.FILE_EXTENSION)).ReadValue(Constants.PROXY.DELAY_NODE, Constants.PROXY.TIME_NAME),
                               Constants.SLUG.COLON,
                               new InitializationFile(String.Concat(Constants.PROXY.FILE_NAME, Constants.PROXY.FILE_EXTENSION)).ReadValue(Constants.PROXY.PAGE_NODE, Constants.PROXY.PAGE_NAME));
        } else { return null; }
      }
      else if (Equals(FLAG, Constants.PROXY.READ))
      {
        if (!String.IsNullOrEmpty(new InitializationFile(String.Concat(Constants.PROXY.FILE_NAME, Constants.PROXY.FILE_EXTENSION)).ReadValue(Constants.PROXY.PROXY_NODE, Constants.PROXY.IP_NAME)) ||
            !String.IsNullOrEmpty(new InitializationFile(String.Concat(Constants.PROXY.FILE_NAME, Constants.PROXY.FILE_EXTENSION)).ReadValue(Constants.PROXY.PROXY_NODE, Constants.PROXY.PORT_NAME)))
        {
          return String.Concat(new InitializationFile(String.Concat(Constants.PROXY.FILE_NAME, Constants.PROXY.FILE_EXTENSION)).ReadValue(Constants.PROXY.PROXY_NODE, Constants.PROXY.IP_NAME),
                               Constants.SLUG.COLON,
                               new InitializationFile(String.Concat(Constants.PROXY.FILE_NAME, Constants.PROXY.FILE_EXTENSION)).ReadValue(Constants.PROXY.PROXY_NODE, Constants.PROXY.PORT_NAME));
        }
        else { return null; }
      }
      else
      {
        new InitializationFile(String.Concat(Constants.PROXY.FILE_NAME, Constants.PROXY.FILE_EXTENSION)).WriteValue(Constants.PROXY.PROXY_NODE, Constants.PROXY.IP_NAME, INFO.Split(Constants.SLUG.COLON.ToCharArray())[0].ToString());
        new InitializationFile(String.Concat(Constants.PROXY.FILE_NAME, Constants.PROXY.FILE_EXTENSION)).WriteValue(Constants.PROXY.PROXY_NODE, Constants.PROXY.PORT_NAME, INFO.Split(Constants.SLUG.COLON.ToCharArray())[1].ToString());
        new InitializationFile(String.Concat(Constants.PROXY.FILE_NAME, Constants.PROXY.FILE_EXTENSION)).WriteValue(Constants.PROXY.DELAY_NODE, Constants.PROXY.TIME_NAME, INFO.Split(Constants.SLUG.COLON.ToCharArray())[2].ToString());
        new InitializationFile(String.Concat(Constants.PROXY.FILE_NAME, Constants.PROXY.FILE_EXTENSION)).WriteValue(Constants.PROXY.PAGE_NODE, Constants.PROXY.PAGE_NAME, INFO.Split(Constants.SLUG.COLON.ToCharArray())[3].ToString());
        return Constants.UNIQUEPOINTER.D;
      }
    }
    public static DataTable GetUserList(String UNIQUE_CODE) { return new DataAccess().SELECT(String.Format(Inquiries.GET_USER_LIST, UNIQUE_CODE)); }
    public static DataTable GetCustomList(String UNIQUE_CODE) { return new DataAccess().SELECT(String.Format(Inquiries.GET_CUSTOM_LIST, UNIQUE_CODE)); }
    public static DataTable GetTemplateList(String UNIQUE_CODE) { return new DataAccess().SELECT(String.Format(Inquiries.GET_TEMPLATE_LIST, UNIQUE_CODE)); }
    public static DataTable GetTemplateTextById(String ID_CODE) { return new DataAccess().SELECT(String.Format(Inquiries.GET_MESSAGE_TEXT_BY_ID, ID_CODE)); }
    public static List<DataRow> GetCustomListMemberByName(String LIST_NAME, String UNIQUE_CODE) { return new DataAccess().SELECT(String.Format(Inquiries.GET_PLAYERS_LIST_BY_NAME, LIST_NAME, UNIQUE_CODE)).Rows.Cast<DataRow>().ToList(); }
    public static void LOADACCOUNTLIST(ComboBox Source)
    {
      try
      {
        DataAccess db = new DataAccess();
        DataTable Receive;
        Receive = db.SELECT(Inquiries.GET_ACCOUNT_LIST);
        Source.DataSource = Receive;
        Source.DisplayMember = "MAGIC_ID";
        Source.ValueMember = "ACCOUNT_PASS";
      }
      catch(Exception ex) { throw new Exception(ex.Message); }
    }
    public static bool INSERTCURRENTINFO(String SERVER, String USERNAME, String PASSWORD)
    {
      if (!Regex.IsMatch(SERVER, Constants.REGEX.TRAVIAN, RegexOptions.IgnoreCase))
      {
        /* آدرس سايت تراوين وارد شده منطبق نيست */
        return false;
      }
      else
      {
        DataAccess DataBase = new DataAccess();
        Dictionary<String, String> INFO = new Dictionary<String, String>();
        INFO.Add("UNIQUE_CODE", String.Concat(USERNAME,
                                              Constants.SLUG.PIPE,
                                              SERVER.Split(Constants.SLUG.DOT.ToCharArray())[0].ToString(),
                                              Constants.SLUG.PIPE,
                                              SERVER.Split(Constants.SLUG.DOT.ToCharArray())[2].ToString()));
        INFO.Add("ACCOUNT_NAME", USERNAME);
        INFO.Add("ACCOUNT_PASS", PASSWORD);
        INFO.Add("ACCOUNT_COUNTRY", Regex.Split(SERVER, String.Concat(Constants.SLUG.DOT, Constants.SLUG.BASIC_STRUCTURE[1].ToString()))[1]);
        INFO.Add("ACCOUNT_SERVER", Regex.Split(SERVER, String.Concat(Constants.SLUG.DOT, Constants.SLUG.BASIC_STRUCTURE[1].ToString()))[0]);
        try
        {
          return DataBase.INSERT(Define.ACCOUNT_LIST, INFO);
        }
        catch(Exception ex)
        {
          if (Equals(ex.Message, Constants.ERRORS.SQLITE_EXCEPTION_CANNOT_INSERT))
          {
            return DataBase.UPDATE(Define.ACCOUNT_LIST, INFO, String.Format("UNIQUE_CODE = '{0}'", String.Concat(USERNAME,
                                                                                                                 Constants.SLUG.PIPE,
                                                                                                                 SERVER.Split(Constants.SLUG.DOT.ToCharArray())[0].ToString(),
                                                                                                                 Constants.SLUG.PIPE,
                                                                                                                 SERVER.Split(Constants.SLUG.DOT.ToCharArray())[2].ToString())));
          }
          else
          {
            return false;
          }
        }
      }
    }
    public static bool INSERTPLAYERLIST(String UNIQUE_CODE, String PLAYERNAME, String SERVER)
    {
      DataAccess DataBase = new DataAccess();
      Dictionary<String, String> INFO = new Dictionary<String, String>();
      INFO.Add("UNIQUE_CODE", UNIQUE_CODE);
      INFO.Add("PLAYER_DESC", PLAYERNAME);
      INFO.Add("RECORD_COUNTRY", Regex.Split(SERVER, String.Concat(Constants.SLUG.DOT, Constants.SLUG.BASIC_STRUCTURE[1].ToString()))[1]);
      INFO.Add("RECORD_SERVER", Regex.Split(SERVER, String.Concat(Constants.SLUG.DOT, Constants.SLUG.BASIC_STRUCTURE[1].ToString()))[0]);
      try
      {
        return DataBase.INSERT(Define.SERVER_USER_LIST, INFO);
      }
      catch
      {
        return false;
      }
    }
    public static bool INSERTCUSTOMLIST(String UNIQUE_CODE, String LIST_NAME, String PLAYERNAME, String SERVER)
    {
      DataAccess DataBase = new DataAccess();
      Dictionary<String, String> INFO = new Dictionary<String, String>();
      INFO.Add("UNIQUE_CODE", UNIQUE_CODE);
      INFO.Add("LIST_NAME", LIST_NAME);
      INFO.Add("PLAYER_DESC", PLAYERNAME);
      INFO.Add("RECORD_COUNTRY", Regex.Split(SERVER, String.Concat(Constants.SLUG.DOT, Constants.SLUG.BASIC_STRUCTURE[1].ToString()))[1]);
      INFO.Add("RECORD_SERVER", Regex.Split(SERVER, String.Concat(Constants.SLUG.DOT, Constants.SLUG.BASIC_STRUCTURE[1].ToString()))[0]);
      try
      {
        return DataBase.INSERT(Define.CUSTOM_LIST, INFO);
      }
      catch
      {
        return false;
      }
    }
    public static bool INSERTTEMPLATEMESSAGE(String UNIQUE_CODE, String MESSAGE_SUBJECT, String MESSAGE_TEXT)
    {
      DataAccess DataBase = new DataAccess();
      Dictionary<String, String> INFO = new Dictionary<String, String>();
      INFO.Add("UNIQUE_CODE", UNIQUE_CODE);
      INFO.Add("MESSAGE_SUBJECT", MESSAGE_SUBJECT);
      INFO.Add("MESSAGE_TEXT", MESSAGE_TEXT);
      try
      {
        return DataBase.INSERT(Define.USER_MESSAGE_TEMPLATE, INFO);
      }
      catch
      {
        return false;
      }
    }
    public static void FetchUsername(String CrudeList, ListBox ListName, Label Message)
    {
      String linealData = null;

      try
      {
        if (ListName.InvokeRequired)
        {
          ListName.Invoke(new MethodInvoker(delegate
          {
            for (int i = 1; i < 21; i++)
            {
              linealData = Regex.Split(CrudeList, Constants.REGEX.UID)[i].ToString();
              ListName.Items.Add(linealData.Substring(0, linealData.IndexOf("</a>")).ToString());
            }
          }));
        }
        else
        {
          for (int i = 1; i < 21; i++)
          {
            linealData = Regex.Split(CrudeList, Constants.REGEX.UID)[i].ToString();
            ListName.Items.Add(linealData.Substring(0, linealData.IndexOf("</a>")).ToString());
          }
        }
      }
      catch
      {
        Utils.MessageDrawer(Message, Constants.COLORS.ERROR, "fail to load and analyze data, please try again");
      }
    }
    public static void DrawComboBoxItems(ComboBox Target)
    {
      DataTable receiveData = new DataTable();

      receiveData = Utils.GetCustomList(String.Concat(Account.cAccount,
                                                            Constants.SLUG.PIPE,
                                                            Account.cServer.Split(Constants.SLUG.DOT.ToCharArray())[Constants.NUMBERS.ZERO].ToString(),
                                                            Constants.SLUG.PIPE,
                                                            Account.cServer.Split(Constants.SLUG.DOT.ToCharArray())[Constants.NUMBERS.TWO].ToString()));
      if (receiveData.Rows.Count > Constants.NUMBERS.ZERO)
      {
        /* اول اطلاعات قبلی کُمبوباکس را پاک می‌کنیم */
        Target.Items.Clear();
        Target.Items.AddRange(new Object[]
        {
          new ComboItem(Constants.KEYS.i10, ResourceManager.Resource.GetString(Constants.WORDS.L)),
          new ComboItem(Constants.KEYS.i20, ResourceManager.Resource.GetString(Constants.WORDS.LI)),
          new ComboItem(Constants.KEYS.i50, ResourceManager.Resource.GetString(Constants.WORDS.LII)),
          new ComboItem(Constants.KEYS.iAL, ResourceManager.Resource.GetString(Constants.WORDS.LIII))
        });
        /* این اکانت دارای لیست گیرنده‌های شخصی‌سازی شده است */
        for (int i = Constants.NUMBERS.ZERO; i < receiveData.Rows.Count; i++)
        {
          // Target.Items.Insert(i + Constants.NUMBERS.FOUR, receiveData.Rows[i][Constants.NUMBERS.ZERO].ToString());
          Target.Items.Insert(i + Constants.NUMBERS.FOUR, new ComboItem(receiveData.Rows[i][Constants.NUMBERS.ZERO].ToString(), receiveData.Rows[i][Constants.NUMBERS.ZERO].ToString()));
        }
      }
      else
      {
        /* این اکانت لیست گیرنده‌های شخصی‌سازی شده ندارد، پس اگر کُمبوباکس بیشتر از ۴ آیتم داشت، باید پاک شود */
        if (Target.Items.Count > Constants.NUMBERS.FOUR)
        {
          Target.Items.Clear();
          Target.Items.AddRange(new Object[]
          {
            new ComboItem(Constants.KEYS.i10, ResourceManager.Resource.GetString(Constants.WORDS.L)),
            new ComboItem(Constants.KEYS.i20, ResourceManager.Resource.GetString(Constants.WORDS.LI)),
            new ComboItem(Constants.KEYS.i50, ResourceManager.Resource.GetString(Constants.WORDS.LII)),
            new ComboItem(Constants.KEYS.iAL, ResourceManager.Resource.GetString(Constants.WORDS.LIII))
          });
        }
      }
    }
    public static void DrawListBoxItems(ListBox Target)
    {
      DataTable receiveData = new DataTable();

      receiveData = Utils.GetTemplateList(String.Concat(Account.cAccount,
                                                              Constants.SLUG.PIPE,
                                                              Account.cServer.Split(Constants.SLUG.DOT.ToCharArray())[Constants.NUMBERS.ZERO].ToString(),
                                                              Constants.SLUG.PIPE,
                                                              Account.cServer.Split(Constants.SLUG.DOT.ToCharArray())[Constants.NUMBERS.TWO].ToString()));
      if (receiveData.Rows.Count > Constants.NUMBERS.ZERO)
      {
        /* اول اطلاعات قبلی لیست پیامهای ذخیره شده را پاک می‌کنیم */
        Target.DataSource = null;
        Target.Items.Clear();
        /* این اکانت دارای لیست پیامهای ذخیره شده است */
        Target.DataSource = receiveData;
        Target.DisplayMember = "MESSAGE_SUBJECT";
        Target.ValueMember = "ID_CODE";
        Target.SelectedIndex = Constants.NUMBERS.MINUS_ONE;
      }
      else
      {
        /* فرد لیست پیامهای ذخیره شده ندارد برای همین لیست را خالی می‌کنیم */
        Target.DataSource = null;
        Target.Items.Clear();
      }
    }
    public static void StatusChanger(TextBox[] tObj, Boolean Status) { StatusChanger(tObj, Status, null, null, null); }
    public static void StatusChanger(TextBox[] tObj, Boolean Status, MaskedTextBox[] mObj) { StatusChanger(tObj, Status, mObj, null, null); }
    public static void StatusChanger(TextBox[] tObj, Boolean Status, MaskedTextBox[] mObj, ComboBox[] cObj) { StatusChanger(tObj, Status, mObj, cObj, null); }
    public static void StatusChanger(TextBox[] tObj, Boolean Status, MaskedTextBox[] mObj, ComboBox[] cObj, Button[] bObj) { for (int i = Constants.NUMBERS.ZERO; i < tObj.Length; i++) { tObj[i].Enabled = Status; } if (mObj != null) { for (int i = Constants.NUMBERS.ZERO; i < mObj.Length; i++) { mObj[i].Enabled = Status; } } if (cObj != null) { for (int i = Constants.NUMBERS.ZERO; i < cObj.Length; i++) { cObj[i].Enabled = Status; } } if (bObj != null) { for (int i = Constants.NUMBERS.ZERO; i < bObj.Length; i++) { bObj[i].Enabled = Status; } } }
    public static List<String> GetValues(String To, String UNIQUE_CODE, ListBox Source)
    {
      List<String> cValues = new List<String>();
      /* int Key = Constants.COMBOBOX.TOP10.Where(KV => KV.Value == ResourceManager.Resource.GetString(Constants.WORDS.L)).Select(KV => KV.Key).FirstOrDefault(); */
      if (Equals(To, Constants.KEYS.i10)) { for (int i = Constants.NUMBERS.ZERO; i < Constants.NUMBERS.TEN; i++) { cValues.Add(Source.GetItemText(Source.Items[i])); } }
      else if (Equals(To, Constants.KEYS.i20)) { for (int i = Constants.NUMBERS.ZERO; i < Constants.NUMBERS.TWENTY; i++) { cValues.Add(Source.GetItemText(Source.Items[i])); } }
      else if (Equals(To, Constants.KEYS.i50)) { for (int i = Constants.NUMBERS.ZERO; i < Constants.NUMBERS.FIFTY; i++) { cValues.Add(Source.GetItemText(Source.Items[i])); } }
      else if (Equals(To, Constants.KEYS.iAL)) { for (int i = Constants.NUMBERS.ZERO; i < Source.Items.Count; i++) { cValues.Add(Source.GetItemText(Source.Items[i])); } }
      else { foreach (DataRow Rows in GetCustomListMemberByName(To, UNIQUE_CODE)) { cValues.Add(Rows[Constants.NUMBERS.ZERO].ToString()); } }
      return cValues;
    }
    /* %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% */
  }
  /* %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% */
  class ComboItem : Object { public String cText, cValue; public ComboItem(String _cValue, String _cText) { cText = _cText; cValue = _cValue; } public override String ToString() { return cText; } }
}
namespace iMS.Resource
{
  public static class ResourceManager
  {
    private static System.Resources.ResourceManager CResource = new System.Resources.ResourceManager(String.Concat("iMS.Languages.", System.Globalization.CultureInfo.CurrentCulture.Name), System.Reflection.Assembly.GetExecutingAssembly());
    public static System.Resources.ResourceManager Resource { get { try { if (String.IsNullOrEmpty(CResource.GetString("ApplicationName"))) { } } catch { CResource = new System.Resources.ResourceManager(String.Concat("iMS.Languages.", "en-US"), System.Reflection.Assembly.GetExecutingAssembly()); } return CResource; } set { CResource = value; } }
  }
}
namespace iMS.Initialization
{
  public class InitializationFile
  {
    private String cPath;
    [DllImport("kernel32")]
    private static extern long WritePrivateProfileString(String Section, String Key, String Val, String FilePath);
    [DllImport("kernel32")]
    private static extern int GetPrivateProfileString(String Section, String Key, String Def, StringBuilder RetVal, int Size, String FilePath);
    public InitializationFile(String iniPath) { cPath = iniPath; }
    public void WriteValue(String Section, String Key, String Value) { WritePrivateProfileString(Section, Key, Value, this.cPath); }
    public String ReadValue(String Section, String Key) { StringBuilder cData = new StringBuilder(255); int i = GetPrivateProfileString(Section, Key, "", cData, 255, this.cPath); return cData.ToString(); }
  }
}