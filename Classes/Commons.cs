﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Text.RegularExpressions;
using System.Threading;
using System.Web;

namespace iMS.Classes
{
  class Commons
  {
    public static CookieContainer SID = new CookieContainer();

    public static void LoadUserList(String URL, System.Windows.Forms.ListBox ListName, System.Windows.Forms.Label Message, Object[] LoginInfo)
    {
      bool FLAG = (Equals(Utils.PROXY(Constants.PROXY.READ), Constants.PROXY.MINI)) ? false : true;

      String PostData = String.Concat("name=",
                                      LoginInfo[Constants.NUMBERS.ZERO],
                                      "&password=",
                                      LoginInfo[Constants.NUMBERS.ONE],
                                      "&login=",
                                      (System.Diagnostics.Stopwatch.GetTimestamp() / 10000));

      String Response = Utils._POSTLOGIN(String.Concat(Utils.URL2URI(URL), "dorf1.php?ok=1"), PostData, FLAG);

      if (Equals(Utils.isExists(Response, "<div class=\"error"), Constants.ERRORS.NOT_EXISTS))
      {
        String[] COOKIEPROPERTY = { Utils.isExists(Response, "t-Cookie: sess_id=", true, ","),
                                    Utils.isExists(Response, "T3E=", true, ";"),
                                    Utils.isExists(Response, "lowRes=", true, ";")};

        SID.Add(new Cookie("sess_id", COOKIEPROPERTY[Constants.NUMBERS.ZERO], Constants.SLUG.SLASH, "travian." + Utils.URL2URI(URL).Authority.ToString().Split('.')[Constants.NUMBERS.TWO]));
        SID.Add(new Cookie("lowRes", COOKIEPROPERTY[Constants.NUMBERS.TWO], Constants.SLUG.SLASH, "travian." + Utils.URL2URI(URL).Authority.ToString().Split('.')[Constants.NUMBERS.TWO]));
        SID.Add(new Cookie("T3E", COOKIEPROPERTY[Constants.NUMBERS.ONE], Constants.SLUG.SLASH, "travian." + Utils.URL2URI(URL).Authority.ToString().Split('.')[Constants.NUMBERS.TWO]));

        Account.cSID = SID;

        String Statistiken = Utils._GETINFO(String.Concat(new Object[] { Utils.URL2URI(URL.ToString()), "statistiken.php?id=0&idSub=0&page=1" }), String.Concat(Utils.URL2URI(URL), "dorf1.php"), Account.cSID, true);

        try
        {
          String lastPage = Regex.Split(Statistiken, Constants.REGEX.LASTPAGE)[Constants.NUMBERS.TWO].ToString();

          lastPage = lastPage.Substring(Constants.NUMBERS.ZERO, lastPage.IndexOf("\" class=\"last\">")).ToString();

          Utils.MessageDrawer(Message, Constants.COLORS.ATTENTION, String.Concat(lastPage, " page loaded"));

          String maxPage = null;

          String INFO = Utils.PROXY(Constants.PROXY.ALL);

          if (!String.IsNullOrEmpty(INFO))
          {
            maxPage = INFO.Split(Constants.SLUG.COLON.ToCharArray())[Constants.NUMBERS.THREE].ToString();
          }

          for (int i = Constants.NUMBERS.ONE; i <= Convert.ToInt32(maxPage); i++)
          {
            Utils.FetchUsername(Utils._GETINFO(String.Concat(new Object[] { Utils.URL2URI(URL.ToString()), "statistiken.php?id=0&idSub=0&page=", i }), String.Concat(Utils.URL2URI(URL), "dorf1.php"), Account.cSID, true), ListName, Message);
            Utils.MessageDrawer(Message, Constants.COLORS.ATTENTION, String.Concat("load server 's players list, please wait ... page ",
                                                                                   i,
                                                                                   " of ",
                                                                                   lastPage,
                                                                                   ""));
          }
          Utils.MessageDrawer(Message, Constants.COLORS.SUCCESS, String.Concat("Done. ",
                                                                               maxPage,
                                                                               " page loaded with successfully"));
        }
        catch (Exception ex) { throw new Exception(ex.Message); }
      }
    }
    /* ^_^_^_^_^_^_^_^_^_^_^_^_^_^_^_^_^_^_^_^_^_^_^_^_^_^_^_^_^_^_^_^_^ */
    public static void SetMessage(String URL, Object[] LoginInfo, List<String> pList, String mSubject, String mBody, System.Windows.Forms.Label Message)
    {
      bool FLAG = (Equals(Utils.PROXY(Constants.PROXY.READ), Constants.PROXY.MINI)) ? false : true;

      String PostData = String.Concat("name=",
                                      LoginInfo[Constants.NUMBERS.ZERO],
                                      "&password=",
                                      LoginInfo[Constants.NUMBERS.ONE],
                                      "&login=",
                                      (System.Diagnostics.Stopwatch.GetTimestamp() / 10000));

      Utils.MessageDrawer(Message, Constants.COLORS.ATTENTION, "Check your login information and try for validate it");

      String Response = Utils._POSTLOGIN(String.Concat(Utils.URL2URI(URL), "dorf1.php?ok=1"), PostData, FLAG);

      if (Equals(Utils.isExists(Response, "<div class=\"error"), Constants.ERRORS.NOT_EXISTS))
      {
        String[] COOKIEPROPERTY = { Utils.isExists(Response, "t-Cookie: sess_id=", true, ","),
                                    Utils.isExists(Response, "T3E=", true, ";"),
                                    Utils.isExists(Response, "lowRes=", true, ";")};

        SID.Add(new Cookie("sess_id", COOKIEPROPERTY[Constants.NUMBERS.ZERO], Constants.SLUG.SLASH, "travian." + Utils.URL2URI(URL).Authority.ToString().Split('.')[Constants.NUMBERS.TWO]));
        SID.Add(new Cookie("lowRes", COOKIEPROPERTY[Constants.NUMBERS.TWO], Constants.SLUG.SLASH, "travian." + Utils.URL2URI(URL).Authority.ToString().Split('.')[Constants.NUMBERS.TWO]));
        SID.Add(new Cookie("T3E", COOKIEPROPERTY[Constants.NUMBERS.ONE], Constants.SLUG.SLASH, "travian." + Utils.URL2URI(URL).Authority.ToString().Split('.')[Constants.NUMBERS.TWO]));

        Account.cSID = SID;

        Utils.MessageDrawer(Message, Constants.COLORS.ATTENTION, "Data initlize for send message, please wait ...");

        String Nachrichten = Utils._GETINFO(String.Concat(new Object[] { Utils.URL2URI(URL.ToString()), "nachrichten.php?t=1" }), String.Concat(Utils.URL2URI(URL), "dorf1.php"), Account.cSID, true);

        try
        {
          String cP = Utils.pValue(Nachrichten, "<input type=\"hidden\" name=\"c\" value=\"", "\" />");

          Utils.MessageDrawer(Message, Constants.COLORS.ATTENTION, String.Concat("Remember: iMS will wait ",
                                                                                 Utils.PROXY(Constants.PROXY.ALL).Split(Constants.SLUG.COLON.ToCharArray())[2].ToString(),
                                                                                 " second for make delay time between each message"));
          for (int i = 0; i < pList.Count; i++)
          {
            String aP = String.Concat("an=", pList[i], "&be=", HttpUtility.UrlEncode(mSubject), "&message=", HttpUtility.UrlEncode(mBody), "&c=", cP);

            Thread.Sleep(int.Parse(Utils.PROXY(Constants.PROXY.ALL).Split(Constants.SLUG.COLON.ToCharArray())[2].ToString()));

            Utils._POST(String.Concat(Utils.URL2URI(URL), "nachrichten.php"), aP, null, true, Account.cSID, FLAG);

            Utils.MessageDrawer(Message, Constants.COLORS.ATTENTION, String.Concat("Your message send to ", pList[i], " with success"));
          }
          Utils.MessageDrawer(Message, Constants.COLORS.SUCCESS, String.Concat("All message send"));
        }
        catch (Exception ex) { throw new Exception(ex.Message); }
      }
    }
  }
}