﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SQLite;

namespace iMS.Classes
{
  class DataAccess
  {
    String ConnectionString = String.Format(Constants.DATABASE.CLASS, Constants.DATABASE.ADDRESS);
    ///
    ///
    ///
    public DataTable SELECT(String sqlCommand)
    {
      DataTable dT = new DataTable();
      try
      {
        SQLiteConnection Connection = new SQLiteConnection(ConnectionString);
        Connection.Open();
        SQLiteCommand Statement = new SQLiteCommand(Connection);
        Statement.CommandText = sqlCommand;
        SQLiteDataReader ResultSet = Statement.ExecuteReader();
        dT.Load(ResultSet);
        ResultSet.Close();
        Connection.Close();
      }
      catch (Exception e) { throw new Exception(e.Message); }
      return dT;
    }
    ///
    ///
    ///
    public string SCALAR(String sqlCommand)
    {
      SQLiteConnection Connection = new SQLiteConnection(ConnectionString);
      Connection.Open();
      SQLiteCommand Statement = new SQLiteCommand(Connection);
      Statement.CommandText = sqlCommand;
      Object Value = Statement.ExecuteScalar();
      Connection.Close();
      if (Value != null)
      {
        return Value.ToString();
      }
      return "";
    }
    ///
    ///
    ///
    public int EXECUTE(String sqlCommand)
    {
      SQLiteConnection Connection = new SQLiteConnection(ConnectionString);
      Connection.Open();
      SQLiteCommand Statement = new SQLiteCommand(Connection);
      Statement.CommandText = sqlCommand;
      int rowsUpdated = Statement.ExecuteNonQuery();
      Connection.Close();
      return rowsUpdated;
    }
    ///
    ///
    ///
    public bool INSERT(String TABLE, Dictionary<String, String> DATA)
    {
      String COLUMNS = "", VALUE = "";

      Boolean FLAG = true;

      foreach (KeyValuePair<String, String> INFO in DATA)
      {
        COLUMNS += String.Format(Constants.QUERY.COLUMNS, INFO.Key.ToString());
        VALUE += String.Format(Constants.QUERY.VALUE, INFO.Value);
      }
      COLUMNS = COLUMNS.Substring(0, COLUMNS.Length - 1);
      VALUE = VALUE.Substring(0, VALUE.Length - 1);
      try
      {
        this.EXECUTE(String.Format(String.Concat(Constants.QUERY.INSERT,
                                                 Constants.QUERY.FIRST,
                                                 Constants.PARENTHESIS.RIGHT,
                                                 Constants.QUERY.SECOND,
                                                 Constants.PARENTHESIS.LEFT,
                                                 Constants.QUERY.VALUES,
                                                 Constants.PARENTHESIS.RIGHT,
                                                 Constants.QUERY.THIRD,
                                                 Constants.PARENTHESIS.LEFT,
                                                 Constants.SLUG.SEMICOLON), TABLE, COLUMNS, VALUE));
      }
      catch (Exception ex)
      {
        if (Equals(ex.Message, Constants.EXCEPTION.UNIQUE_KEY))
        {
          throw new SQLiteException(Constants.ERRORS.SQLITE_EXCEPTION_CANNOT_INSERT);
        }
        else
        {
          FLAG = false;
        }
      }
      return FLAG;
    }
    ///
    ///
    ///
    public bool UPDATE(String TABLE, Dictionary<String, String> DATA, String WHERE)
    {
      String VALUES = "";

      Boolean CODE = true;

      if (DATA.Count >= 1)
      {
        foreach (KeyValuePair<String, String> INFO in DATA)
        {
          VALUES += String.Format(String.Concat(Constants.SLUG.SPACE,
                                                Constants.QUERY.FIRST,
                                                " = '",
                                                Constants.QUERY.SECOND,
                                                "',"), INFO.Key.ToString(), INFO.Value.ToString());
        }
        VALUES = VALUES.Substring(0, VALUES.Length - 1);
      }
      try
      {
        this.EXECUTE(String.Format(String.Concat(Constants.QUERY.UPDATE,
                                                 Constants.QUERY.FIRST,
                                                 Constants.QUERY.SET,
                                                 Constants.QUERY.SECOND,
                                                 Constants.QUERY.WHERE,
                                                 Constants.QUERY.THIRD,
                                                 Constants.SLUG.SEMICOLON), TABLE, VALUES, WHERE));
      }
      catch
      {
        CODE = false;
      }
      return CODE;
    }
    ///
    ///
    ///
    public bool DELETE(String TABLE, String WHERE)
    {
      Boolean CODE = true;
      try
      {
        this.EXECUTE(String.Format(String.Concat(Constants.QUERY.DELETE,
                                                 Constants.QUERY.FIRST,
                                                 Constants.QUERY.WHERE,
                                                 Constants.QUERY.SECOND,
                                                 Constants.SLUG.SEMICOLON), TABLE, WHERE));
      }
      catch
      {
        CODE = false;
      }
      return CODE;
    }
    ///
    ///
    ///
  }
}