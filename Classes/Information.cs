﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;

namespace iMS.Classes
{
  public static class Account
  {
    static String SERVER = "";

    public static String cServer
    {
      get { return Account.SERVER; }
      set { Account.SERVER = value; }
    }
    static String ACCOUNT = "";

    public static String cAccount
    {
      get { return Account.ACCOUNT; }
      set { Account.ACCOUNT = value; }
    }
    static CookieContainer SID = null;

    public static CookieContainer cSID
    {
      get { return Account.SID; }
      set { Account.SID = value; }
    }
  }
}