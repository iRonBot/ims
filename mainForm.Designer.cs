﻿using System;
using System.Reflection;
using iMS.Resource;
using iMS.Classes;
using System.Windows.Forms;
namespace iMS
{
  partial class mainForm
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(mainForm));
      this.mainMenu = new System.Windows.Forms.MenuStrip();
      this.fileMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.exitMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.toolsMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.settingsMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.thirdSeparator = new System.Windows.Forms.ToolStripSeparator();
      this.createMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.helpMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.faceBookMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.homePageMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.firstSeparator = new System.Windows.Forms.ToolStripSeparator();
      this.getSourceMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.secondSeparator = new System.Windows.Forms.ToolStripSeparator();
      this.aboutMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.ServerLabel = new System.Windows.Forms.Label();
      this.ServerValue = new System.Windows.Forms.TextBox();
      this.AccountLabel = new System.Windows.Forms.Label();
      this.AccountValue = new System.Windows.Forms.TextBox();
      this.PasswordLabel = new System.Windows.Forms.Label();
      this.PasswordValue = new System.Windows.Forms.MaskedTextBox();
      this.Connect = new System.Windows.Forms.CheckBox();
      this.Save = new System.Windows.Forms.Button();
      this.SavedAccountList = new System.Windows.Forms.ComboBox();
      this.mainProgressBar = new System.Windows.Forms.ProgressBar();
      this.mainPanel = new System.Windows.Forms.Panel();
      this.SaveTemplate = new System.Windows.Forms.Button();
      this.Send = new System.Windows.Forms.Button();
      this.TemplateList = new System.Windows.Forms.Label();
      this.QuoteKey = new System.Windows.Forms.PictureBox();
      this.ImageKey = new System.Windows.Forms.PictureBox();
      this.HypeLinkKey = new System.Windows.Forms.PictureBox();
      this.EmailKey = new System.Windows.Forms.PictureBox();
      this.IndentKey = new System.Windows.Forms.PictureBox();
      this.RightKey = new System.Windows.Forms.PictureBox();
      this.CenterKey = new System.Windows.Forms.PictureBox();
      this.LeftKey = new System.Windows.Forms.PictureBox();
      this.SizeKey = new System.Windows.Forms.PictureBox();
      this.HighLightKey = new System.Windows.Forms.PictureBox();
      this.FontFamilyKey = new System.Windows.Forms.PictureBox();
      this.FontColorKey = new System.Windows.Forms.PictureBox();
      this.UnderLineKey = new System.Windows.Forms.PictureBox();
      this.ItalicKey = new System.Windows.Forms.PictureBox();
      this.BoldKey = new System.Windows.Forms.PictureBox();
      this.SubjectLabel = new System.Windows.Forms.Label();
      this.SubjectValue = new System.Windows.Forms.TextBox();
      this.ToValue = new System.Windows.Forms.ComboBox();
      this.ToLabel = new System.Windows.Forms.Label();
      this.ComposeMessage = new System.Windows.Forms.Label();
      this.MessageTemplateList = new System.Windows.Forms.ListBox();
      this.MessageBody = new System.Windows.Forms.RichTextBox();
      this.UserListSave = new System.Windows.Forms.Button();
      this.UserListLoad = new System.Windows.Forms.Button();
      this.ServerUserListLabel = new System.Windows.Forms.Label();
      this.ServerUserList = new System.Windows.Forms.ListBox();
      this.HeaderLogo = new System.Windows.Forms.PictureBox();
      this.SecondSeparatorPicture = new System.Windows.Forms.PictureBox();
      this.FirstSeparatorPicture = new System.Windows.Forms.PictureBox();
      this.mainMessage = new System.Windows.Forms.Label();

      this.mainMenu.SuspendLayout();
      this.mainPanel.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.QuoteKey)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.ImageKey)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.HypeLinkKey)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.EmailKey)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.IndentKey)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.RightKey)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.CenterKey)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.LeftKey)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.SizeKey)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.HighLightKey)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.FontFamilyKey)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.FontColorKey)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.UnderLineKey)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.ItalicKey)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.BoldKey)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.HeaderLogo)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.SecondSeparatorPicture)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.FirstSeparatorPicture)).BeginInit();
      this.SuspendLayout();
      // 
      // mainMenu
      // 
      this.mainMenu.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
      this.mainMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileMenuItem,
            this.toolsMenuItem,
            this.helpMenuItem});
      this.mainMenu.Location = new System.Drawing.Point(0, 0);
      this.mainMenu.Name = "mainMenu";
      this.mainMenu.Size = new System.Drawing.Size(642, 24);
      this.mainMenu.Text = "mainMenu";
      // 
      // fileMenuItem
      // 
      this.fileMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.exitMenuItem});
      this.fileMenuItem.Name = "fileMenuItem";
      this.fileMenuItem.Size = new System.Drawing.Size(35, 20);
      this.fileMenuItem.Text = ResourceManager.Resource.GetString(Constants.WORDS.II);
      // 
      // exitMenuItem
      // 
      this.exitMenuItem.Name = "exitMenuItem";
      this.exitMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.F4)));
      this.exitMenuItem.Size = new System.Drawing.Size(132, 22);
      this.exitMenuItem.Text = ResourceManager.Resource.GetString(Constants.WORDS.III);
      this.exitMenuItem.Click += new System.EventHandler(this.Exit_Click);
      // 
      // toolsMenuItem
      // 
      this.toolsMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.settingsMenuItem,
            this.thirdSeparator,
            this.createMenuItem});
      this.toolsMenuItem.Name = "toolsMenuItem";
      this.toolsMenuItem.Size = new System.Drawing.Size(44, 20);
      this.toolsMenuItem.Text = ResourceManager.Resource.GetString(Constants.WORDS.IV);
      // 
      // settingsMenuItem
      // 
      this.settingsMenuItem.Image = global::iMS.Properties.Resources.Settings;
      this.settingsMenuItem.Name = "settingsMenuItem";
      this.settingsMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.S)));
      this.settingsMenuItem.Size = new System.Drawing.Size(256, 22);
      this.settingsMenuItem.Text = ResourceManager.Resource.GetString(Constants.WORDS.V);
      this.settingsMenuItem.Click += new System.EventHandler(this.Settings_Click);
      // 
      // thirdSeparator
      // 
      this.thirdSeparator.Name = "thirdSeparator";
      this.thirdSeparator.Size = new System.Drawing.Size(253, 6);
      // 
      // createMenuItem
      // 
      this.createMenuItem.Image = global::iMS.Properties.Resources.Users;
      this.createMenuItem.Name = "createMenuItem";
      this.createMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.Shift)
                  | System.Windows.Forms.Keys.C)));
      this.createMenuItem.Size = new System.Drawing.Size(256, 22);
      this.createMenuItem.Text = ResourceManager.Resource.GetString(Constants.WORDS.VI);
      this.createMenuItem.Click += new System.EventHandler(this.CCR_Click);
      // 
      // helpMenuItem
      // 
      this.helpMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.faceBookMenuItem,
            this.homePageMenuItem,
            this.firstSeparator,
            this.getSourceMenuItem,
            this.secondSeparator,
            this.aboutMenuItem});
      this.helpMenuItem.Name = "helpMenuItem";
      this.helpMenuItem.Size = new System.Drawing.Size(40, 20);
      this.helpMenuItem.Text = ResourceManager.Resource.GetString(Constants.WORDS.VII);
      // 
      // faceBookMenuItem
      // 
      this.faceBookMenuItem.Image = global::iMS.Properties.Resources.Facebook;
      this.faceBookMenuItem.Name = "faceBookMenuItem";
      this.faceBookMenuItem.Size = new System.Drawing.Size(175, 22);
      this.faceBookMenuItem.Text = ResourceManager.Resource.GetString(Constants.WORDS.VIII);
      this.faceBookMenuItem.Click += new System.EventHandler(this.Support_Click);
      // 
      // homePageMenuItem
      // 
      this.homePageMenuItem.Image = global::iMS.Properties.Resources.Home;
      this.homePageMenuItem.Name = "homePageMenuItem";
      this.homePageMenuItem.Size = new System.Drawing.Size(175, 22);
      this.homePageMenuItem.Text = ResourceManager.Resource.GetString(Constants.WORDS.IX);
      this.homePageMenuItem.Click += new System.EventHandler(this.Home_Click);
      // 
      // firstSeparator
      // 
      this.firstSeparator.Name = "firstSeparator";
      this.firstSeparator.Size = new System.Drawing.Size(172, 6);
      // 
      // getSourceMenuItem
      // 
      this.getSourceMenuItem.Image = global::iMS.Properties.Resources.Get_Source;
      this.getSourceMenuItem.Name = "getSourceMenuItem";
      this.getSourceMenuItem.Size = new System.Drawing.Size(175, 22);
      this.getSourceMenuItem.Text = ResourceManager.Resource.GetString(Constants.WORDS.X);
      this.getSourceMenuItem.Click += new System.EventHandler(this.Source_Click);
      // 
      // secondSeparator
      // 
      this.secondSeparator.Name = "secondSeparator";
      this.secondSeparator.Size = new System.Drawing.Size(172, 6);
      // 
      // aboutMenuItem
      // 
      this.aboutMenuItem.Name = "aboutMenuItem";
      this.aboutMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F1;
      this.aboutMenuItem.Size = new System.Drawing.Size(175, 22);
      this.aboutMenuItem.Text = ResourceManager.Resource.GetString(Constants.WORDS.XI);
      this.aboutMenuItem.Click += new System.EventHandler(this.About_Click);
      // 
      // ServerLabel
      // 
      this.ServerLabel.AutoSize = true;
      this.ServerLabel.Location = new System.Drawing.Point(5, 33);
      this.ServerLabel.Name = "ServerLabel";
      this.ServerLabel.Size = new System.Drawing.Size(43, 13);
      this.ServerLabel.Text = (Equals(Constants.CULTURE.CURRENT, Constants.SLUG.BASIC_STRUCTURE[2])) ? String.Concat(ResourceManager.Resource.GetString(Constants.WORDS.XXIX), Constants.SLUG.COLON) : String.Concat(Constants.SLUG.COLON, ResourceManager.Resource.GetString(Constants.WORDS.XXIX));
      // 
      // ServerValue
      // 
      this.ServerValue.Location = new System.Drawing.Point(47, 30);
      this.ServerValue.Name = "ServerValue";
      this.ServerValue.Size = new System.Drawing.Size(100, 21);
      this.ServerValue.TabIndex = 0;
      // 
      // AccountLabel
      // 
      this.AccountLabel.AutoSize = true;
      this.AccountLabel.Location = new System.Drawing.Point(150, 33);
      this.AccountLabel.Name = "AccountLabel";
      this.AccountLabel.Size = new System.Drawing.Size(59, 13);
      this.AccountLabel.Text = (Equals(Constants.CULTURE.CURRENT, Constants.SLUG.BASIC_STRUCTURE[2])) ? String.Concat(ResourceManager.Resource.GetString(Constants.WORDS.XXX), Constants.SLUG.COLON) : String.Concat(Constants.SLUG.COLON, ResourceManager.Resource.GetString(Constants.WORDS.XXX));
      // 
      // AccountValue
      // 
      this.AccountValue.Location = new System.Drawing.Point(208, 30);
      this.AccountValue.Name = "AccountValue";
      this.AccountValue.Size = new System.Drawing.Size(100, 21);
      this.AccountValue.TabIndex = 1;
      // 
      // PasswordLabel
      // 
      this.PasswordLabel.AutoSize = true;
      this.PasswordLabel.Location = new System.Drawing.Point(311, 33);
      this.PasswordLabel.Name = "PasswordLabel";
      this.PasswordLabel.Size = new System.Drawing.Size(57, 13);
      this.PasswordLabel.Text = (Equals(Constants.CULTURE.CURRENT, Constants.SLUG.BASIC_STRUCTURE[2])) ? String.Concat(ResourceManager.Resource.GetString(Constants.WORDS.XXXI), Constants.SLUG.COLON) : String.Concat(Constants.SLUG.COLON, ResourceManager.Resource.GetString(Constants.WORDS.XXXI));
      // 
      // PasswordValue
      // 
      this.PasswordValue.Location = new System.Drawing.Point(367, 30);
      this.PasswordValue.Name = "PasswordValue";
      this.PasswordValue.Size = new System.Drawing.Size(100, 21);
      this.PasswordValue.PasswordChar = '●';
      this.PasswordValue.TabIndex = 2;
      // 
      // Connect
      // 
      this.Connect.Appearance = System.Windows.Forms.Appearance.Button;
      this.Connect.Cursor = System.Windows.Forms.Cursors.Hand;
      this.Connect.Image = global::iMS.Properties.Resources.DisConnect;
      this.Connect.Location = new System.Drawing.Point(468, 29);
      this.Connect.Name = "Connect";
      this.Connect.Size = new System.Drawing.Size(23, 23);
      this.Connect.UseVisualStyleBackColor = true;
      this.Connect.Click += new System.EventHandler(this.Connect_Click);
      this.Connect.TabIndex = 3;
      // 
      // Save
      // 
      this.Save.Cursor = System.Windows.Forms.Cursors.Hand;
      this.Save.Image = global::iMS.Properties.Resources.Save;
      this.Save.Location = new System.Drawing.Point(490, 29);
      this.Save.Name = "Save";
      this.Save.Size = new System.Drawing.Size(23, 23);
      this.Save.UseVisualStyleBackColor = true;
      this.Save.Click += new System.EventHandler(this.SaveServerList_Click);
      this.Save.TabIndex = 4;
      // 
      // SavedAccountList
      // 
      this.SavedAccountList.Location = new System.Drawing.Point(514, 30);
      this.SavedAccountList.Name = "SavedAccountList";
      this.SavedAccountList.Size = new System.Drawing.Size(121, 21);
      this.SavedAccountList.DropDownStyle = ComboBoxStyle.DropDownList;
      this.SavedAccountList.TabIndex = 5;
      this.SavedAccountList.SelectionChangeCommitted += new System.EventHandler(this.SavedAccountList_Changed);
      // 
      // mainProgressBar
      // 
      this.mainProgressBar.Location = new System.Drawing.Point(2, 536);
      this.mainProgressBar.Name = "mainProgressBar";
      this.mainProgressBar.Size = new System.Drawing.Size(638, 23);
      // 
      // mainMessage
      // 
      this.mainMessage.Location = new System.Drawing.Point(342, 516);
      this.mainMessage.Name = "mainMessage";
      this.mainMessage.AutoSize = true;
      // 
      // mainPanel
      // 
      this.mainPanel.BackColor = System.Drawing.SystemColors.Control;
      // this.mainPanel.Controls.Add(this.mainMessage);
      this.Controls.Add(this.mainMessage);
      this.mainPanel.Controls.Add(this.SaveTemplate);
      this.mainPanel.Controls.Add(this.Send);
      this.mainPanel.Controls.Add(this.TemplateList);
      this.mainPanel.Controls.Add(this.QuoteKey);
      this.mainPanel.Controls.Add(this.ImageKey);
      this.mainPanel.Controls.Add(this.HypeLinkKey);
      this.mainPanel.Controls.Add(this.EmailKey);
      this.mainPanel.Controls.Add(this.IndentKey);
      this.mainPanel.Controls.Add(this.RightKey);
      this.mainPanel.Controls.Add(this.CenterKey);
      this.mainPanel.Controls.Add(this.LeftKey);
      this.mainPanel.Controls.Add(this.SizeKey);
      this.mainPanel.Controls.Add(this.HighLightKey);
      this.mainPanel.Controls.Add(this.FontFamilyKey);
      this.mainPanel.Controls.Add(this.FontColorKey);
      this.mainPanel.Controls.Add(this.UnderLineKey);
      this.mainPanel.Controls.Add(this.ItalicKey);
      this.mainPanel.Controls.Add(this.BoldKey);
      this.mainPanel.Controls.Add(this.SubjectLabel);
      this.mainPanel.Controls.Add(this.SubjectValue);
      this.mainPanel.Controls.Add(this.ToValue);
      this.mainPanel.Controls.Add(this.ToLabel);
      this.mainPanel.Controls.Add(this.ComposeMessage);
      this.mainPanel.Controls.Add(this.MessageTemplateList);
      this.mainPanel.Controls.Add(this.MessageBody);
      this.mainPanel.Controls.Add(this.UserListSave);
      this.mainPanel.Controls.Add(this.UserListLoad);
      this.mainPanel.Controls.Add(this.ServerUserListLabel);
      this.mainPanel.Controls.Add(this.ServerUserList);

      this.mainPanel.Enabled = false;
      this.mainPanel.Location = new System.Drawing.Point(2, 168);
      this.mainPanel.Name = "mainPanel";
      this.mainPanel.Size = new System.Drawing.Size(638, 366);
      // 
      // SaveTemplate
      // 
      this.SaveTemplate.Location = new System.Drawing.Point(236, 343);
      this.SaveTemplate.Name = "SaveTemplate";
      this.SaveTemplate.AutoSize = true;
      this.SaveTemplate.Text = ResourceManager.Resource.GetString(Constants.WORDS.XXXVIII);
      this.SaveTemplate.UseVisualStyleBackColor = true;
      this.SaveTemplate.Click += new System.EventHandler(this.SaveTemplate_Click);
      // 
      // Send
      // 
      this.Send.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
      this.Send.Location = new System.Drawing.Point(183, 343);
      this.Send.Name = "Send";
      this.Send.Size = new System.Drawing.Size(50, 23);
      this.Send.Text = ResourceManager.Resource.GetString(Constants.WORDS.XXXVII);
      this.Send.UseVisualStyleBackColor = true;
      this.Send.Click += new System.EventHandler(this.SendMessage);
      // 
      // TemplateList
      // 
      this.TemplateList.AutoSize = true;
      this.TemplateList.Location = new System.Drawing.Point(-3, 190);
      this.TemplateList.Name = "TemplateList";
      this.TemplateList.Size = new System.Drawing.Size(110, 13);
      this.TemplateList.Text = ResourceManager.Resource.GetString(Constants.WORDS.XXXIII);
      // 
      // QuoteKey
      // 
      this.QuoteKey.Cursor = System.Windows.Forms.Cursors.Hand;
      this.QuoteKey.ErrorImage = null;
      this.QuoteKey.Image = global::iMS.Properties.Resources.Quote;
      this.QuoteKey.InitialImage = null;
      // this.QuoteKey.Location = new System.Drawing.Point(449, 65);
      this.QuoteKey.Location = new System.Drawing.Point(259, 65);
      this.QuoteKey.Name = "QuoteKey";
      this.QuoteKey.Size = new System.Drawing.Size(16, 16);
      this.QuoteKey.Click += new System.EventHandler(this.QuoteEvent);
      // 
      // ImageKey
      // 
      this.ImageKey.Cursor = System.Windows.Forms.Cursors.Hand;
      this.ImageKey.ErrorImage = null;
      this.ImageKey.Image = global::iMS.Properties.Resources.Image;
      this.ImageKey.InitialImage = null;
      this.ImageKey.Location = new System.Drawing.Point(430, 65);
      this.ImageKey.Name = "ImageKey";
      this.ImageKey.Size = new System.Drawing.Size(16, 16);
      this.ImageKey.Visible = false;
      // 
      // HypeLinkKey
      // 
      this.HypeLinkKey.Cursor = System.Windows.Forms.Cursors.Hand;
      this.HypeLinkKey.ErrorImage = null;
      this.HypeLinkKey.Image = global::iMS.Properties.Resources.Hyperlink;
      this.HypeLinkKey.InitialImage = null;
      this.HypeLinkKey.Location = new System.Drawing.Point(411, 65);
      this.HypeLinkKey.Name = "HypeLinkKey";
      this.HypeLinkKey.Size = new System.Drawing.Size(16, 16);
      this.HypeLinkKey.Visible = false;
      // 
      // EmailKey
      // 
      this.EmailKey.Cursor = System.Windows.Forms.Cursors.Hand;
      this.EmailKey.ErrorImage = null;
      this.EmailKey.Image = global::iMS.Properties.Resources.Email;
      this.EmailKey.InitialImage = null;
      this.EmailKey.Location = new System.Drawing.Point(392, 65);
      this.EmailKey.Name = "EmailKey";
      this.EmailKey.Size = new System.Drawing.Size(16, 16);
      this.EmailKey.Visible = false;
      // 
      // IndentKey
      // 
      this.IndentKey.Cursor = System.Windows.Forms.Cursors.Hand;
      this.IndentKey.ErrorImage = null;
      this.IndentKey.Image = global::iMS.Properties.Resources.Indent;
      this.IndentKey.InitialImage = null;
      this.IndentKey.Location = new System.Drawing.Point(373, 65);
      this.IndentKey.Name = "IndentKey";
      this.IndentKey.Size = new System.Drawing.Size(16, 16);
      this.IndentKey.Visible = false;
      // 
      // RightKey
      // 
      this.RightKey.Cursor = System.Windows.Forms.Cursors.Hand;
      this.RightKey.ErrorImage = null;
      this.RightKey.Image = global::iMS.Properties.Resources.Right;
      this.RightKey.InitialImage = null;
      this.RightKey.Location = new System.Drawing.Point(354, 65);
      this.RightKey.Name = "RightKey";
      this.RightKey.Size = new System.Drawing.Size(16, 16);
      this.RightKey.Visible = false;
      // 
      // CenterKey
      // 
      this.CenterKey.Cursor = System.Windows.Forms.Cursors.Hand;
      this.CenterKey.ErrorImage = null;
      this.CenterKey.Image = global::iMS.Properties.Resources.Center;
      this.CenterKey.InitialImage = null;
      this.CenterKey.Location = new System.Drawing.Point(335, 65);
      this.CenterKey.Name = "CenterKey";
      this.CenterKey.Size = new System.Drawing.Size(16, 16);
      this.CenterKey.Visible = false;
      // 
      // LeftKey
      // 
      this.LeftKey.Cursor = System.Windows.Forms.Cursors.Hand;
      this.LeftKey.ErrorImage = null;
      this.LeftKey.Image = global::iMS.Properties.Resources.Left;
      this.LeftKey.InitialImage = null;
      this.LeftKey.Location = new System.Drawing.Point(316, 65);
      this.LeftKey.Name = "LeftKey";
      this.LeftKey.Size = new System.Drawing.Size(16, 16);
      this.LeftKey.Visible = false;
      // 
      // SizeKey
      // 
      this.SizeKey.Cursor = System.Windows.Forms.Cursors.Hand;
      this.SizeKey.ErrorImage = null;
      this.SizeKey.Image = global::iMS.Properties.Resources.Size;
      this.SizeKey.InitialImage = null;
      this.SizeKey.Location = new System.Drawing.Point(297, 65);
      this.SizeKey.Name = "SizeKey";
      this.SizeKey.Size = new System.Drawing.Size(16, 16);
      this.SizeKey.Visible = false;
      // 
      // HighLightKey
      // 
      this.HighLightKey.Cursor = System.Windows.Forms.Cursors.Hand;
      this.HighLightKey.ErrorImage = null;
      this.HighLightKey.Image = global::iMS.Properties.Resources.HighLight;
      this.HighLightKey.InitialImage = null;
      this.HighLightKey.Location = new System.Drawing.Point(278, 65);
      this.HighLightKey.Name = "HighLightKey";
      this.HighLightKey.Size = new System.Drawing.Size(16, 16);
      this.HighLightKey.Visible = false;
      // 
      // FontFamilyKey
      // 
      this.FontFamilyKey.Cursor = System.Windows.Forms.Cursors.Hand;
      this.FontFamilyKey.ErrorImage = null;
      this.FontFamilyKey.Image = global::iMS.Properties.Resources.Font_Family;
      this.FontFamilyKey.InitialImage = null;
      this.FontFamilyKey.Location = new System.Drawing.Point(259, 65);
      this.FontFamilyKey.Name = "FontFamilyKey";
      this.FontFamilyKey.Size = new System.Drawing.Size(16, 16);
      this.FontFamilyKey.Visible = false;
      // 
      // FontColorKey
      // 
      this.FontColorKey.Cursor = System.Windows.Forms.Cursors.Hand;
      this.FontColorKey.ErrorImage = null;
      this.FontColorKey.Image = global::iMS.Properties.Resources.Color;
      this.FontColorKey.InitialImage = null;
      this.FontColorKey.Location = new System.Drawing.Point(240, 65);
      this.FontColorKey.Name = "FontColorKey";
      this.FontColorKey.Size = new System.Drawing.Size(16, 16);
      this.FontColorKey.Click += new System.EventHandler(this.ColorEvent);
      // 
      // UnderLineKey
      // 
      this.UnderLineKey.Cursor = System.Windows.Forms.Cursors.Hand;
      this.UnderLineKey.ErrorImage = null;
      this.UnderLineKey.Image = global::iMS.Properties.Resources.UnderLine;
      this.UnderLineKey.InitialImage = null;
      this.UnderLineKey.Location = new System.Drawing.Point(221, 65);
      this.UnderLineKey.Name = "UnderLineKey";
      this.UnderLineKey.Size = new System.Drawing.Size(16, 16);
      this.UnderLineKey.Click += new System.EventHandler(this.UnderlineEvent);
      // 
      // ItalicKey
      // 
      this.ItalicKey.Cursor = System.Windows.Forms.Cursors.Hand;
      this.ItalicKey.ErrorImage = null;
      this.ItalicKey.Image = global::iMS.Properties.Resources.Italic;
      this.ItalicKey.InitialImage = null;
      this.ItalicKey.Location = new System.Drawing.Point(202, 65);
      this.ItalicKey.Name = "ItalicKey";
      this.ItalicKey.Size = new System.Drawing.Size(16, 16);
      this.ItalicKey.Click += new System.EventHandler(this.ItalicEvent);
      // 
      // BoldKey
      // 
      this.BoldKey.Cursor = System.Windows.Forms.Cursors.Hand;
      this.BoldKey.ErrorImage = null;
      this.BoldKey.Image = global::iMS.Properties.Resources.Bold;
      this.BoldKey.InitialImage = null;
      this.BoldKey.Location = new System.Drawing.Point(183, 65);
      this.BoldKey.Name = "BoldKey";
      this.BoldKey.Size = new System.Drawing.Size(16, 16);
      this.BoldKey.Click += new System.EventHandler(this.BoldEvent);
      // 
      // SubjectLabel
      // 
      this.SubjectLabel.Location = new System.Drawing.Point(182, 45);
      this.SubjectLabel.Name = "SubjectLabel";
      this.SubjectLabel.Size = new System.Drawing.Size(47, 13);
      this.SubjectLabel.Text = (Equals(Constants.CULTURE.CURRENT, Constants.SLUG.BASIC_STRUCTURE[2])) ? String.Concat(ResourceManager.Resource.GetString(Constants.WORDS.XXXVI), Constants.SLUG.COLON) : String.Concat(Constants.SLUG.COLON, ResourceManager.Resource.GetString(Constants.WORDS.XXXVI));
      // 
      // SubjectValue
      // 
      this.SubjectValue.Location = new System.Drawing.Point(229, 42);
      this.SubjectValue.Name = "SubjectValue";
      this.SubjectValue.Size = new System.Drawing.Size(409, 21);
      // 
      // ToValue
      // 
      this.ToValue.FormattingEnabled = true;
      this.ToValue.Location = new System.Drawing.Point(204, 20);
      this.ToValue.Name = "ToValue";
      this.ToValue.DropDownStyle = ComboBoxStyle.DropDownList;
      this.ToValue.Size = new System.Drawing.Size(434, 21);
      this.ToValue.Items.AddRange(new Object[]
      {
        new ComboItem(Constants.KEYS.i10, ResourceManager.Resource.GetString(Constants.WORDS.L)),
        new ComboItem(Constants.KEYS.i20, ResourceManager.Resource.GetString(Constants.WORDS.LI)),
        new ComboItem(Constants.KEYS.i50, ResourceManager.Resource.GetString(Constants.WORDS.LII)),
        new ComboItem(Constants.KEYS.iAL, ResourceManager.Resource.GetString(Constants.WORDS.LIII))
      });
      // 
      // ToLabel
      // 
      this.ToLabel.Location = new System.Drawing.Point(182, 23);
      this.ToLabel.Name = "ToLabel";
      this.ToLabel.Size = new System.Drawing.Size(23, 13);
      this.ToLabel.Text = (Equals(Constants.CULTURE.CURRENT, Constants.SLUG.BASIC_STRUCTURE[2])) ? String.Concat(ResourceManager.Resource.GetString(Constants.WORDS.XXXV), Constants.SLUG.COLON) : String.Concat(Constants.SLUG.COLON, ResourceManager.Resource.GetString(Constants.WORDS.XXXV));
      // 
      // ComposeMessage
      // 
      this.ComposeMessage.AutoSize = true;
      this.ComposeMessage.Location = new System.Drawing.Point(180, 3);
      this.ComposeMessage.Name = "ComposeMessage";
      this.ComposeMessage.Size = new System.Drawing.Size(65, 13);
      this.ComposeMessage.Text = ResourceManager.Resource.GetString(Constants.WORDS.XXXIV);
      // 
      // MessageTemplateList
      // 
      this.MessageTemplateList.FormattingEnabled = true;
      this.MessageTemplateList.Location = new System.Drawing.Point(0, 206);
      this.MessageTemplateList.Name = "MessageTemplateList";
      this.MessageTemplateList.ScrollAlwaysVisible = true;
      this.MessageTemplateList.Size = new System.Drawing.Size(180, 160);
      this.MessageTemplateList.Click += new System.EventHandler(this.TemplateList_Click);
      // 
      // MessageBody
      // 
      this.MessageBody.Location = new System.Drawing.Point(183, 84);
      this.MessageBody.Margin = new System.Windows.Forms.Padding(5);
      this.MessageBody.MaxLength = 9804;
      this.MessageBody.Name = "MessageBody";
      this.MessageBody.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.ForcedVertical;
      this.MessageBody.Size = new System.Drawing.Size(455, 258);
      this.MessageBody.DetectUrls = false;
      // 
      // UserListSave
      // 
      this.UserListSave.Cursor = System.Windows.Forms.Cursors.Hand;
      this.UserListSave.Image = global::iMS.Properties.Resources.Save;
      this.UserListSave.Location = new System.Drawing.Point(25, 167);
      this.UserListSave.Name = "UserListSave";
      this.UserListSave.Size = new System.Drawing.Size(23, 23);
      this.UserListSave.UseVisualStyleBackColor = true;
      this.UserListSave.Click += new System.EventHandler(this.SavePlayerList_Click);
      // 
      // UserListLoad
      // 
      this.UserListLoad.Cursor = System.Windows.Forms.Cursors.Hand;
      this.UserListLoad.Image = global::iMS.Properties.Resources.Reload;
      this.UserListLoad.Location = new System.Drawing.Point(-1, 167);
      this.UserListLoad.Name = "UserListLoad";
      this.UserListLoad.Size = new System.Drawing.Size(23, 23);
      this.UserListLoad.UseVisualStyleBackColor = true;
      this.UserListLoad.Click += new System.EventHandler(this.ReloadPlayerList_Click);
      // 
      // ServerUserListLabel
      // 
      this.ServerUserListLabel.AutoSize = true;
      this.ServerUserListLabel.Location = new System.Drawing.Point(-3, 3);
      this.ServerUserListLabel.Name = "ServerUserListLabel";
      this.ServerUserListLabel.Size = new System.Drawing.Size(72, 13);
      this.ServerUserListLabel.Text = ResourceManager.Resource.GetString(Constants.WORDS.XXXII);
      // 
      // ServerUserList
      // 
      this.ServerUserList.FormattingEnabled = true;
      this.ServerUserList.Location = new System.Drawing.Point(0, 19);
      this.ServerUserList.Name = "ServerUserList";
      this.ServerUserList.ScrollAlwaysVisible = true;
      this.ServerUserList.Size = new System.Drawing.Size(180, 147);
      // 
      // HeaderLogo
      // 
      this.HeaderLogo.ErrorImage = null;
      this.HeaderLogo.Image = global::iMS.Properties.Resources.HeaderLogo_fa_IR;
      this.HeaderLogo.InitialImage = null;
      this.HeaderLogo.Location = new System.Drawing.Point(2, 58);
      this.HeaderLogo.Name = "HeaderLogo";
      this.HeaderLogo.Size = new System.Drawing.Size(638, 109);
      this.HeaderLogo.TabStop = false;
      // 
      // SecondSeparatorPicture
      // 
      this.SecondSeparatorPicture.ErrorImage = null;
      this.SecondSeparatorPicture.Image = global::iMS.Properties.Resources.Separator;
      this.SecondSeparatorPicture.InitialImage = null;
      this.SecondSeparatorPicture.Location = new System.Drawing.Point(2, 25);
      this.SecondSeparatorPicture.Name = "SecondSeparatorPicture";
      this.SecondSeparatorPicture.Size = new System.Drawing.Size(638, 2);
      this.SecondSeparatorPicture.TabStop = false;
      // 
      // FirstSeparatorPicture
      // 
      this.FirstSeparatorPicture.ErrorImage = null;
      this.FirstSeparatorPicture.Image = global::iMS.Properties.Resources.Separator;
      this.FirstSeparatorPicture.InitialImage = null;
      this.FirstSeparatorPicture.Location = new System.Drawing.Point(2, 54);
      this.FirstSeparatorPicture.Name = "FirstSeparatorPicture";
      this.FirstSeparatorPicture.Size = new System.Drawing.Size(638, 2);
      this.FirstSeparatorPicture.TabStop = false;
      // 
      // mainForm
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(642, 562);
      this.Controls.Add(this.HeaderLogo);
      this.Controls.Add(this.mainPanel);
      this.Controls.Add(this.mainProgressBar);
      this.Controls.Add(this.SecondSeparatorPicture);
      this.Controls.Add(this.FirstSeparatorPicture);
      this.Controls.Add(this.SavedAccountList);
      this.Controls.Add(this.Save);
      this.Controls.Add(this.Connect);
      this.Controls.Add(this.PasswordValue);
      this.Controls.Add(this.PasswordLabel);
      this.Controls.Add(this.AccountValue);
      this.Controls.Add(this.AccountLabel);
      this.Controls.Add(this.ServerValue);
      this.Controls.Add(this.ServerLabel);
      this.Controls.Add(this.mainMenu);
      this.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
      this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
      this.MainMenuStrip = this.mainMenu;
      this.MaximizeBox = false;
      this.Name = "mainForm";
      this.RightToLeft = Utils.Direction(ResourceManager.Resource.GetString("IsRightToLeft"));
      this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
      this.Text = ResourceManager.Resource.GetString(Constants.WORDS.I);
      this.mainMenu.ResumeLayout(false);
      this.mainMenu.PerformLayout();
      this.mainPanel.ResumeLayout(false);
      this.mainPanel.PerformLayout();
      ((System.ComponentModel.ISupportInitialize)(this.QuoteKey)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.ImageKey)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.HypeLinkKey)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.EmailKey)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.IndentKey)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.RightKey)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.CenterKey)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.LeftKey)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.SizeKey)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.HighLightKey)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.FontFamilyKey)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.FontColorKey)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.UnderLineKey)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.ItalicKey)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.BoldKey)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.HeaderLogo)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.SecondSeparatorPicture)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.FirstSeparatorPicture)).EndInit();
      this.ResumeLayout(false);
      this.PerformLayout();
    }
    #endregion

    private System.Windows.Forms.MenuStrip mainMenu;
    private System.Windows.Forms.ToolStripMenuItem fileMenuItem;
    private System.Windows.Forms.ToolStripMenuItem exitMenuItem;
    private System.Windows.Forms.ToolStripMenuItem toolsMenuItem;
    private System.Windows.Forms.ToolStripMenuItem settingsMenuItem;
    private System.Windows.Forms.ToolStripMenuItem createMenuItem;
    private System.Windows.Forms.ToolStripMenuItem helpMenuItem;
    private System.Windows.Forms.ToolStripMenuItem faceBookMenuItem;
    private System.Windows.Forms.ToolStripMenuItem homePageMenuItem;
    private System.Windows.Forms.ToolStripSeparator firstSeparator;
    private System.Windows.Forms.ToolStripMenuItem getSourceMenuItem;
    private System.Windows.Forms.ToolStripSeparator secondSeparator;
    private System.Windows.Forms.ToolStripMenuItem aboutMenuItem;
    private System.Windows.Forms.Label ServerLabel;
    private System.Windows.Forms.TextBox ServerValue;
    private System.Windows.Forms.Label AccountLabel;
    private System.Windows.Forms.TextBox AccountValue;
    private System.Windows.Forms.Label PasswordLabel;
    private System.Windows.Forms.MaskedTextBox PasswordValue;
    private System.Windows.Forms.CheckBox Connect;
    private System.Windows.Forms.Button Save;
    private System.Windows.Forms.ComboBox SavedAccountList;
    private System.Windows.Forms.PictureBox FirstSeparatorPicture;
    private System.Windows.Forms.PictureBox SecondSeparatorPicture;
    private System.Windows.Forms.ProgressBar mainProgressBar;
    private System.Windows.Forms.Panel mainPanel;
    private System.Windows.Forms.PictureBox HeaderLogo;
    private System.Windows.Forms.ListBox ServerUserList;
    private System.Windows.Forms.Label ServerUserListLabel;
    private System.Windows.Forms.Button UserListLoad;
    private System.Windows.Forms.Button UserListSave;
    private System.Windows.Forms.RichTextBox MessageBody;
    private System.Windows.Forms.ListBox MessageTemplateList;
    private System.Windows.Forms.Label ComposeMessage;
    private System.Windows.Forms.Label ToLabel;
    private System.Windows.Forms.ComboBox ToValue;
    private System.Windows.Forms.TextBox SubjectValue;
    private System.Windows.Forms.Label SubjectLabel;
    private System.Windows.Forms.PictureBox BoldKey;
    private System.Windows.Forms.PictureBox ItalicKey;
    private System.Windows.Forms.PictureBox UnderLineKey;
    private System.Windows.Forms.PictureBox FontColorKey;
    private System.Windows.Forms.PictureBox FontFamilyKey;
    private System.Windows.Forms.PictureBox HighLightKey;
    private System.Windows.Forms.PictureBox SizeKey;
    private System.Windows.Forms.PictureBox LeftKey;
    private System.Windows.Forms.PictureBox RightKey;
    private System.Windows.Forms.PictureBox CenterKey;
    private System.Windows.Forms.PictureBox IndentKey;
    private System.Windows.Forms.PictureBox HypeLinkKey;
    private System.Windows.Forms.PictureBox EmailKey;
    private System.Windows.Forms.PictureBox ImageKey;
    private System.Windows.Forms.PictureBox QuoteKey;
    private System.Windows.Forms.Label TemplateList;
    private System.Windows.Forms.Button Send;
    private System.Windows.Forms.Button SaveTemplate;
    private System.Windows.Forms.ToolStripSeparator thirdSeparator;
    private System.Windows.Forms.Label mainMessage;

  }
}