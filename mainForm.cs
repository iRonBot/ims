﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Diagnostics;
using System.Data.SQLite;
using iMS.Classes;
using iMS.Tables;
using System.Text.RegularExpressions;
using System.Net.NetworkInformation;
using System.Threading;
using iMS.Resource;

namespace iMS
{
  public partial class mainForm : Form
  {
    private static Uri URI;

    public mainForm()
    {
      InitializeComponent();
      InitializeForm();
    }

    private void InitializeForm()
    {
      try { Utils.LOADACCOUNTLIST(SavedAccountList); }
      catch(Exception ex) { if (Equals(ex.Message, Constants.EXCEPTION.DATABASE_NOT_FOUND)) { Utils.MessageDrawer(mainMessage, Constants.COLORS.ERROR, "Database not found, please download latest version of iMS"); } }
      /// بارگذاری اکانت‌های ذخیره شده در پایگاه داده
      if(SavedAccountList.Items.Count > Constants.NUMBERS.ZERO)
      {
        ServerValue.Text = String.Concat(SavedAccountList.Text.Split(Constants.SLUG.PIPE.ToCharArray())[Constants.NUMBERS.ZERO],
                                                                     Constants.SLUG.DOT,
                                                                     Constants.SLUG.BASIC_STRUCTURE[Constants.NUMBERS.ONE].ToString(),
                                         SavedAccountList.Text.Split(Constants.SLUG.PIPE.ToCharArray())[Constants.NUMBERS.TWO]).ToLower();
        AccountValue.Text = SavedAccountList.Text.Split(Constants.SLUG.PIPE.ToCharArray())[Constants.NUMBERS.ONE];
        PasswordValue.Text = SavedAccountList.SelectedValue.ToString();
      }
      /// بارگذاری اطلاعات پروکسی و میزان وقفه‌ی تنظیم شده در برنامه
      if(!Utils.FileExists()) { Utils.PROXY(Constants.PROXY.WRITE, Constants.PROXY.FULL); }
      ///
    }

    private void SavedAccountList_Changed(object sender, EventArgs e)
    {
      ServerValue.Text = String.Concat(SavedAccountList.Text.Split(Constants.SLUG.PIPE.ToCharArray())[Constants.NUMBERS.ZERO],
                                                                   Constants.SLUG.DOT,
                                                                   Constants.SLUG.BASIC_STRUCTURE[Constants.NUMBERS.ONE].ToString(),
                                       SavedAccountList.Text.Split(Constants.SLUG.PIPE.ToCharArray())[Constants.NUMBERS.TWO]).ToLower();
      AccountValue.Text = SavedAccountList.Text.Split(Constants.SLUG.PIPE.ToCharArray())[Constants.NUMBERS.ONE];
      PasswordValue.Text = SavedAccountList.SelectedValue.ToString();
    }

    private void Connect_Click(object sender, EventArgs e)
    {
      Thread ConnectThread = null;

      if (!Regex.IsMatch(ServerValue.Text, Constants.REGEX.TRAVIAN, RegexOptions.IgnoreCase))
      {
        /* آدرس سايت تراوين وارد شده منطبق نيست */
        Utils.MessageDrawer(mainMessage, Color.BlueViolet, "Wrong Travian Address");
      }
      else
      {
        URI = Utils.URL2URI(Regex.Match(ServerValue.Text.ToLower(), Constants.REGEX.TRAVIAN).ToString());

        if (!NetworkInterface.GetIsNetworkAvailable())
        {
          /* شبکه در دسترس نیست */
        }
        else
        {
          bool FLAG = (Equals(Utils.PROXY(Constants.PROXY.READ), Constants.PROXY.MINI)) ? false : true;
          bool IsConnect = false;

          if (!Utils.isValidURL(URI.ToString(), FLAG))
          {
            /* به دلایلی دسترسی به آدرس وارد شده ممکن نیست، از فیلترشکن استفاده کنید */
          }
          else
          {
            /* در حال بارگذاری اطلاعات شما */
            if (base.InvokeRequired)
            {
              base.Invoke(new MethodInvoker(delegate
              {
                if (Connect.Checked)
                {
                  /* به سرور متصل شد */
                  Connect.Image = global::iMS.Properties.Resources.Connect;
                  Utils.StatusChanger(new TextBox[] { ServerValue, AccountValue }, false, new MaskedTextBox[] { PasswordValue }, new ComboBox[] { SavedAccountList }, new Button[] { Save });
                  IsConnect = true;
                }
                else
                {
                  /* ارتباط به سرور قطع شد */
                  Connect.Image = global::iMS.Properties.Resources.DisConnect;
                  Utils.StatusChanger(new TextBox[] { ServerValue, AccountValue }, true, new MaskedTextBox[] { PasswordValue }, new ComboBox[] { SavedAccountList }, new Button[] { Save });
                  IsConnect = false;
                }
              }));
            }
            else
            {
              if (Connect.Checked)
              {
                /* به سرور متصل شد */
                Connect.Image = global::iMS.Properties.Resources.Connect;
                Utils.StatusChanger(new TextBox[] { ServerValue, AccountValue }, false, new MaskedTextBox[] { PasswordValue }, new ComboBox[] { SavedAccountList }, new Button[] { Save });
                IsConnect = true;
              }
              else
              {
                /* ارتباط به سرور قطع شد */
                Connect.Image = global::iMS.Properties.Resources.DisConnect;
                Utils.StatusChanger(new TextBox[] { ServerValue, AccountValue }, true, new MaskedTextBox[] { PasswordValue }, new ComboBox[] { SavedAccountList }, new Button[] { Save });
                IsConnect = false;
              }
            }
            if (IsConnect)
            {
              /* تنظیم اطلاعات لاگین در بخش عمومی اکانت */
              Account.cServer = ServerValue.Text.ToLower().ToString();
              Account.cAccount = AccountValue.Text;
              /* اضافه‌شدن لیست گیرنده‌های شخصی‌سازی شده در بخش ارسال */
              Utils.DrawComboBoxItems(ToValue);
              /* اضافه‌شدن لیست پیامهای ذخیره شده به ازای هر اکانت */
              Utils.DrawListBoxItems(MessageTemplateList);
              /* فعال کردن پنل اصلی برنامه به معنای اتصال به سرور */
              this.mainPanel.Enabled = true;
              /* پاک کردن محتویات لیست اعضاء سرور */
              ServerUserList.DataSource = null;
              ServerUserList.Items.Clear();
              /* انتخاب ۱٠ نفر برتر سرور از اطلاعات کُمبوباکس */
              ToValue.SelectedIndex = Constants.NUMBERS.ZERO;
              /* شروع قلب برنامه */
              ConnectThread = new Thread(new ThreadStart(doTask));
              ConnectThread.IsBackground = true;
              ConnectThread.Start();
            }
            else
            {
              /* پاک کردن متن پیام احتمالی که در حال نمایش است */
              Utils.MessageDrawer(mainMessage, Constants.COLORS.NONE, Constants.SLUG.EMPTY);
              /* غیرفعال کردن پنل اصلی برنامه به معنای قطع ارتباط از سرور */
              this.mainPanel.Enabled = false;
              /* پاک کردن محتویات لیست اعضاء سرور */
              ServerUserList.DataSource = null;
              ServerUserList.Items.Clear();
              /* پاک‌کردن اطلاعات احتمالی موجود در لیست پیامهای ذخیره شده */
              MessageTemplateList.DataSource = null;
              MessageTemplateList.Items.Clear();
              /* انتخاب گزینه‌ی خالی از اطلاعات کُمبوباکس */
              ToValue.SelectedIndex = Constants.NUMBERS.MINUS_ONE;
            }
          }
        }
      }
    }

    private void doTask()
    {
      DataTable dT = new DataTable();
      DialogResult dR = new DialogResult();

      dT = Utils.GetUserList(String.Concat(AccountValue.Text,
                                                 Constants.SLUG.PIPE,
                                                 ServerValue.Text.Split(Constants.SLUG.DOT.ToCharArray())[Constants.NUMBERS.ZERO].ToString(),
                                                 Constants.SLUG.PIPE,
                                                 ServerValue.Text.Split(Constants.SLUG.DOT.ToCharArray())[Constants.NUMBERS.TWO].ToString()));
      if (dT.Rows.Count > Constants.NUMBERS.ZERO)
      {
        Utils.MessageDrawer(mainMessage, Constants.COLORS.NONE, Constants.SLUG.SPACE);

        MessageBoxManager.Yes = ResourceManager.Resource.GetString(Constants.WORDS.XLIII);
        MessageBoxManager.No = ResourceManager.Resource.GetString(Constants.WORDS.XLIV);
        if (base.InvokeRequired)
        {
          base.Invoke(new MethodInvoker(delegate
          {
            MessageBoxManager.Register();
            dR = MessageBox.Show(this, ResourceManager.Resource.GetString(Constants.WORDS.XLV), ResourceManager.Resource.GetString(Constants.WORDS.XLVI), MessageBoxButtons.YesNo);
            MessageBoxManager.Unregister();
          }));
        }
        else
        {
          MessageBoxManager.Register();
          dR = MessageBox.Show(this, ResourceManager.Resource.GetString(Constants.WORDS.XLV), ResourceManager.Resource.GetString(Constants.WORDS.XLVI), MessageBoxButtons.YesNo);
          MessageBoxManager.Unregister();
        }
        if (Equals(dR.ToString().ToLower(), Constants.BOOLEAN.YES))
        {
          ServerUserList.DataSource = dT;
          if (ServerUserList.InvokeRequired)
          {
            ServerUserList.Invoke(new MethodInvoker(delegate
            {
              ServerUserList.DisplayMember = "PLAYER_DESC";
              ServerUserList.ValueMember = "ID_CODE";
            }));
          }
          else
          {
            ServerUserList.DisplayMember = "PLAYER_DESC";
            ServerUserList.ValueMember = "ID_CODE";
          }
        }
        else
        {
          try
          {
            Commons.LoadUserList(ServerValue.Text,
                                 ServerUserList,
                                 mainMessage,
                                 new Object[] { AccountValue.Text, PasswordValue.Text });
          }
          catch (Exception ex)
          {
            if (Equals(ex.Message, Constants.EXCEPTION.INDEX_OUTSIDE))
            {
              Utils.MessageDrawer(mainMessage, Constants.COLORS.ERROR, "Travian Maintenance work");
            }
            else
            {
              Utils.MessageDrawer(mainMessage, Constants.COLORS.ERROR, "Unknown Exception, Check Internet Connection");
            }
            if (base.InvokeRequired)
            {
              base.Invoke(new MethodInvoker(delegate
              {
                Connect.Image = global::iMS.Properties.Resources.DisConnect;
                Utils.StatusChanger(new TextBox[] { ServerValue, AccountValue }, true, new MaskedTextBox[] { PasswordValue }, new ComboBox[] { SavedAccountList }, new Button[] { Save });
                Connect.Checked = false;
                /* غیرفعال کردن پنل اصلی برنامه به معنای قطع ارتباط از سرور */
                this.mainPanel.Enabled = false;
                /* پاک کردن محتویات لیست اعضاء سرور */
                ServerUserList.DataSource = null;
                ServerUserList.Items.Clear();
                /* انتخاب گزینه‌ی خالی از اطلاعات کُمبوباکس */
                ToValue.SelectedIndex = Constants.NUMBERS.MINUS_ONE;
              }));
            }
            else
            {
              Connect.Image = global::iMS.Properties.Resources.DisConnect;
              Utils.StatusChanger(new TextBox[] { ServerValue, AccountValue }, true, new MaskedTextBox[] { PasswordValue }, new ComboBox[] { SavedAccountList }, new Button[] { Save });
              Connect.Checked = false;
              /* غیرفعال کردن پنل اصلی برنامه به معنای قطع ارتباط از سرور */
              this.mainPanel.Enabled = false;
              /* پاک کردن محتویات لیست اعضاء سرور */
              ServerUserList.DataSource = null;
              ServerUserList.Items.Clear();
              /* انتخاب گزینه‌ی خالی از اطلاعات کُمبوباکس */
              ToValue.SelectedIndex = Constants.NUMBERS.MINUS_ONE;
            }
          }
        }
      }
      else
      {
        try
        {
          Commons.LoadUserList(ServerValue.Text,
                               ServerUserList,
                               mainMessage,
                               new Object[] { AccountValue.Text, PasswordValue.Text });
        }
        catch (Exception ex)
        {
          if (Equals(ex.Message, Constants.EXCEPTION.INDEX_OUTSIDE))
          {
            Utils.MessageDrawer(mainMessage, Constants.COLORS.ERROR, "Travian Maintenance work");
          }
          else
          {
            Utils.MessageDrawer(mainMessage, Constants.COLORS.ERROR, "Unknown Exception, Check Internet Connection");
          }
          if (base.InvokeRequired)
          {
            base.Invoke(new MethodInvoker(delegate
            {
              Connect.Image = global::iMS.Properties.Resources.DisConnect;
              Utils.StatusChanger(new TextBox[] { ServerValue, AccountValue }, true, new MaskedTextBox[] { PasswordValue }, new ComboBox[] { SavedAccountList }, new Button[] { Save });
              Connect.Checked = false;
              /* غیرفعال کردن پنل اصلی برنامه به معنای قطع ارتباط از سرور */
              this.mainPanel.Enabled = false;
              /* پاک کردن محتویات لیست اعضاء سرور */
              ServerUserList.DataSource = null;
              ServerUserList.Items.Clear();
              /* انتخاب گزینه‌ی خالی از اطلاعات کُمبوباکس */
              ToValue.SelectedIndex = Constants.NUMBERS.MINUS_ONE;
            }));
          }
          else
          {
            Connect.Image = global::iMS.Properties.Resources.DisConnect;
            Utils.StatusChanger(new TextBox[] { ServerValue, AccountValue }, true, new MaskedTextBox[] { PasswordValue }, new ComboBox[] { SavedAccountList }, new Button[] { Save });
            Connect.Checked = false;
            /* غیرفعال کردن پنل اصلی برنامه به معنای قطع ارتباط از سرور */
            this.mainPanel.Enabled = false;
            /* پاک کردن محتویات لیست اعضاء سرور */
            ServerUserList.DataSource = null;
            ServerUserList.Items.Clear();
            /* انتخاب گزینه‌ی خالی از اطلاعات کُمبوباکس */
            ToValue.SelectedIndex = Constants.NUMBERS.MINUS_ONE;
          }
        }
      }
    }

    private void Settings_Click(object sender, EventArgs e)
    {
      new settingsForm().ShowDialog(this);
    }

    private void CCR_Click(object sender, EventArgs e)
    {
      /* تنظیم اطلاعات لاگین در بخش عمومی اکانت */
      Account.cServer = ServerValue.Text.ToLower().ToString();
      Account.cAccount = AccountValue.Text;
      /* نمایش پنجره‌ی ساخت لیست گیرنده‌های شخصی‌سازی شده */
      new createCustomReceiverForm().ShowDialog(this);
    }

    private void Exit_Click(object sender, EventArgs e)
    {
      Application.Exit();
    }

    private void Support_Click(object sender, EventArgs e)
    {
      Process.Start("https://www.facebook.com/pages/iRonBot/117042505083782");
    }

    private void Home_Click(object sender, EventArgs e)
    {
      Process.Start("http://ironbot.ir/1391/12/26/ims/");
    }

    private void Source_Click(object sender, EventArgs e)
    {
      Process.Start("https://bitbucket.org/iRonBot/ims");
    }

    private void About_Click(object sender, EventArgs e)
    {
      /* نمایش پنجره‌ی درباره‌ی نرم‌افزار */
      new aboutBox().ShowDialog(this);
    }

    private void SaveServerList_Click(object sender, EventArgs e)
    {
      if (Utils.INSERTCURRENTINFO(ServerValue.Text, AccountValue.Text, PasswordValue.Text))
      {
        Utils.LOADACCOUNTLIST(SavedAccountList);
      }
      else
      {
        /* در ذخیره‌سازی اطلاعات مشکلی رخ داده است */
      }
    }

    private void ReloadPlayerList_Click(object sender, EventArgs e)
    {
      Thread ConnectThread = null;

      if (!Regex.IsMatch(ServerValue.Text, Constants.REGEX.TRAVIAN, RegexOptions.IgnoreCase))
      {
        /* آدرس سايت تراوين وارد شده منطبق نيست */
      }
      else
      {
        URI = Utils.URL2URI(Regex.Match(ServerValue.Text.ToLower(), Constants.REGEX.TRAVIAN).ToString());

        if (!NetworkInterface.GetIsNetworkAvailable())
        {
          /* شبکه در دسترس نیست */
        }
        else
        {
          bool FLAG = (Equals(Utils.PROXY(Constants.PROXY.READ), Constants.PROXY.MINI)) ? false : true;

          if (!Utils.isValidURL(URI.ToString(), FLAG))
          {
            /* به دلایلی دسترسی به آدرس وارد شده ممکن نیست، از فیلترشکن استفاده کنید */
          }
          else
          {
            /* در حال بارگذاری اطلاعات شما */
            if (base.InvokeRequired)
            {
              base.Invoke(new MethodInvoker(delegate
              {
                if (Connect.Checked)
                {
                  /* به سرور متصل شد */
                  Connect.Image = global::iMS.Properties.Resources.Connect;
                  Utils.StatusChanger(new TextBox[] { ServerValue, AccountValue }, false, new MaskedTextBox[] { PasswordValue }, new ComboBox[] { SavedAccountList }, new Button[] { Save });
                }
                else
                {
                  /* ارتباط به سرور قطع شد */
                  Connect.Image = global::iMS.Properties.Resources.DisConnect;
                  Utils.StatusChanger(new TextBox[] { ServerValue, AccountValue }, true, new MaskedTextBox[] { PasswordValue }, new ComboBox[] { SavedAccountList }, new Button[] { Save });
                }
              }));
            }
            else
            {
              if (Connect.Checked)
              {
                /* به سرور متصل شد */
                Connect.Image = global::iMS.Properties.Resources.Connect;
                Utils.StatusChanger(new TextBox[] { ServerValue, AccountValue }, false, new MaskedTextBox[] { PasswordValue }, new ComboBox[] { SavedAccountList }, new Button[] { Save });
              }
              else
              {
                /* ارتباط به سرور قطع شد */
                Connect.Image = global::iMS.Properties.Resources.DisConnect;
                Utils.StatusChanger(new TextBox[] { ServerValue, AccountValue }, true, new MaskedTextBox[] { PasswordValue }, new ComboBox[] { SavedAccountList }, new Button[] { Save });
              }
            }
            /* نمایش پیام بارگذاری مجدد اطلاعات */
            Utils.MessageDrawer(mainMessage, Constants.COLORS.ATTENTION, "Reload player list, please wait ...");
            /* پاک کردن محتویات لیست اعضاء سرور */
            ServerUserList.DataSource = null;
            ServerUserList.Items.Clear();
            /* شروع قلب برنامه */
            ConnectThread = new Thread(new ThreadStart(doTask));
            ConnectThread.IsBackground = true;
            ConnectThread.Start();
          }
        }
      }
    }

    private void SavePlayerList_Click(object sender, EventArgs e)
    {
      new DataAccess().DELETE(Define.SERVER_USER_LIST, String.Format("UNIQUE_CODE = '{0}'", String.Concat(AccountValue.Text,
                                                                                                          Constants.SLUG.PIPE,
                                                                                                          ServerValue.Text.Split(Constants.SLUG.DOT.ToCharArray())[Constants.NUMBERS.ZERO].ToString(),
                                                                                                          Constants.SLUG.PIPE,
                                                                                                          ServerValue.Text.Split(Constants.SLUG.DOT.ToCharArray())[Constants.NUMBERS.TWO].ToString())));
      for (int i = Constants.NUMBERS.ZERO; i < ServerUserList.Items.Count; i++)
      {
        /* وضعیت صحت درج اطلاعات را در این نسخه بررسی نمی‌کنم */
        Utils.INSERTPLAYERLIST(String.Concat(AccountValue.Text,
                                             Constants.SLUG.PIPE,
                                             ServerValue.Text.Split(Constants.SLUG.DOT.ToCharArray())[Constants.NUMBERS.ZERO].ToString(),
                                             Constants.SLUG.PIPE,
                                             ServerValue.Text.Split(Constants.SLUG.DOT.ToCharArray())[Constants.NUMBERS.TWO].ToString()),
                                             ServerUserList.GetItemText(ServerUserList.Items[i]),
                                             ServerValue.Text);
      }
      Utils.MessageDrawer(mainMessage, Constants.COLORS.SUCCESS, "Current player 's list of this account saved successfully");
    }

    private void SendMessage(object sender, EventArgs e)
    {
      DialogResult dR = new DialogResult();
      /* TODO */
      if (ServerUserList.Text.Trim().Length == Constants.NUMBERS.ZERO)
      {
        /* لیست گیرنده‌های پیام نمی‌تواند خالی باشد */
      }
      else
      {
        if (SubjectValue.Text.Trim().Length == Constants.NUMBERS.ZERO
            ||
            MessageBody.Text.Trim().Length == Constants.NUMBERS.ZERO)
        {
          /* عنوان نامه یا متن آن خالی است، برای اطمینان باید سوال پرسید */
          MessageBoxManager.Yes = ResourceManager.Resource.GetString(Constants.WORDS.XLIII);
          MessageBoxManager.No = ResourceManager.Resource.GetString(Constants.WORDS.XLIV);
          if (base.InvokeRequired)
          {
            base.Invoke(new MethodInvoker(delegate
            {
              MessageBoxManager.Register();
              dR = MessageBox.Show("Send this message without a subject or text in the body ?", "Attention", MessageBoxButtons.YesNo);
              MessageBoxManager.Unregister();
            }));
          }
          else
          {
            MessageBoxManager.Register();
            dR = MessageBox.Show("Send this message without a subject or text in the body ?", "Attention", MessageBoxButtons.YesNo);
            MessageBoxManager.Unregister();
          }
          if (Equals(dR.ToString().ToLower(), Constants.BOOLEAN.YES))
          {
            /* لیست کاربرانی که می‌باید به آنها نامه ارسال شود */
            List<String> Players = Utils.GetValues(((ComboItem)(ToValue.SelectedItem)).cValue, String.Concat(AccountValue.Text,
                                                                                                             Constants.SLUG.PIPE,
                                                                                                             ServerValue.Text.Split(Constants.SLUG.DOT.ToCharArray())[Constants.NUMBERS.ZERO].ToString(),
                                                                                                             Constants.SLUG.PIPE,
                                                                                                             ServerValue.Text.Split(Constants.SLUG.DOT.ToCharArray())[Constants.NUMBERS.TWO].ToString()), ServerUserList);

            Commons.SetMessage(ServerValue.Text,
                               new Object[] { AccountValue.Text, PasswordValue.Text },
                               Players,
                               SubjectValue.Text.ToString(),
                               MessageBody.Text.ToString(),
                               mainMessage);
          }
          else
          {
            /* خوب اگر مطمئن نیستی چرا اصلا نامه رو ارسال کردی؟ */
          }
        }
        else
        {
          /* لیست کاربرانی که می‌باید به آنها نامه ارسال شود */
          List<String> Players = Utils.GetValues(((ComboItem)(ToValue.SelectedItem)).cValue, String.Concat(AccountValue.Text,
                                                                                                           Constants.SLUG.PIPE,
                                                                                                           ServerValue.Text.Split(Constants.SLUG.DOT.ToCharArray())[Constants.NUMBERS.ZERO].ToString(),
                                                                                                           Constants.SLUG.PIPE,
                                                                                                           ServerValue.Text.Split(Constants.SLUG.DOT.ToCharArray())[Constants.NUMBERS.TWO].ToString()), ServerUserList);

          Commons.SetMessage(ServerValue.Text,
                             new Object[] { AccountValue.Text, PasswordValue.Text },
                             Players,
                             SubjectValue.Text.ToString(),
                             MessageBody.Text.ToString(),
                             mainMessage);
        }
      }
    }

    private void SaveTemplate_Click(object sender, EventArgs e)
    {
      if (SubjectValue.Text.Trim().Length == Constants.NUMBERS.ZERO
          ||
          MessageBody.Text.Trim().Length == Constants.NUMBERS.ZERO)
      {
        Utils.MessageDrawer(mainMessage, Constants.COLORS.ERROR, "Fill Data");
      }
      else
      {
        if (Utils.INSERTTEMPLATEMESSAGE(String.Concat(AccountValue.Text,
                                                      Constants.SLUG.PIPE,
                                                      ServerValue.Text.Split(Constants.SLUG.DOT.ToCharArray())[Constants.NUMBERS.ZERO].ToString(),
                                                      Constants.SLUG.PIPE,
                                                      ServerValue.Text.Split(Constants.SLUG.DOT.ToCharArray())[Constants.NUMBERS.TWO].ToString()),
                                        SubjectValue.Text,
                                        MessageBody.Text))
        {
          /* اضافه‌شدن لیست پیامهای ذخیره شده به ازای هر اکانت */
          Utils.DrawListBoxItems(MessageTemplateList);
        }
        else
        {
          /* در ذخیره‌سازی اطلاعات مشکلی رخ داده است */
        }
      }
    }

    private void TemplateList_Click(object sender, EventArgs e)
    {
      /* قبل از نمایش پیام ذخیره شده، پیام احتمالی قبلی را پاک می‌کنیم */
      MessageBody.Clear();
      /* درج پیام ذخیره شده در بدنه‌ی اصلی */
      MessageBody.AppendText(Utils.GetTemplateTextById(MessageTemplateList.SelectedValue.ToString()).Rows[Constants.NUMBERS.ZERO][Constants.NUMBERS.ZERO].ToString());
    }

    private void BoldEvent(object sender, EventArgs e) { MessageBody.AppendText(Constants.WYSIWYG.BOLD); }

    private void ItalicEvent(object sender, EventArgs e) { MessageBody.AppendText(Constants.WYSIWYG.ITALIC); }

    private void UnderlineEvent(object sender, EventArgs e) { MessageBody.AppendText(Constants.WYSIWYG.UNDERLINE); }

    private void ColorEvent(object sender, EventArgs e) { ColorDialog cPicker = new ColorDialog(); cPicker.AllowFullOpen = false; if (cPicker.ShowDialog(this) != DialogResult.Cancel) { MessageBody.AppendText(String.Format(Constants.WYSIWYG.COLOR, cPicker.Color.Name.ToLower())); } }

    private void QuoteEvent(object sender, EventArgs e) { MessageBody.AppendText(Constants.WYSIWYG.QUOTE); }
  }
}