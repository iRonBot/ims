﻿using iMS.Resource;
using iMS.Classes;

namespace iMS
{
  partial class aboutBox
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(aboutBox));
      this.tableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
      this.AboutLogo = new System.Windows.Forms.PictureBox();
      this.ProductNameLabel = new System.Windows.Forms.Label();
      this.Version = new System.Windows.Forms.Label();
      this.Copyright = new System.Windows.Forms.Label();
      this.CompanyNameLabel = new System.Windows.Forms.Label();
      this.Description = new System.Windows.Forms.TextBox();
      this.OKButton = new System.Windows.Forms.Button();
      this.tableLayoutPanel.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.AboutLogo)).BeginInit();
      this.SuspendLayout();
      // 
      // tableLayoutPanel
      // 
      this.tableLayoutPanel.ColumnCount = 2;
      this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33F));
      this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 67F));
      this.tableLayoutPanel.Controls.Add(this.AboutLogo, 0, 0);
      this.tableLayoutPanel.Controls.Add(this.ProductNameLabel, 1, 0);
      this.tableLayoutPanel.Controls.Add(this.Version, 1, 1);
      this.tableLayoutPanel.Controls.Add(this.Copyright, 1, 2);
      this.tableLayoutPanel.Controls.Add(this.CompanyNameLabel, 1, 3);
      this.tableLayoutPanel.Controls.Add(this.Description, 1, 4);
      this.tableLayoutPanel.Controls.Add(this.OKButton, 1, 5);
      this.tableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
      this.tableLayoutPanel.Location = new System.Drawing.Point(9, 9);
      this.tableLayoutPanel.Name = "tableLayoutPanel";
      this.tableLayoutPanel.RowCount = 6;
      this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
      this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
      this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
      this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
      this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
      this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
      this.tableLayoutPanel.Size = new System.Drawing.Size(417, 265);
      this.tableLayoutPanel.RightToLeft = Utils.Direction(ResourceManager.Resource.GetString("IsRightToLeft"));
      // 
      // AboutLogo
      // 
      this.AboutLogo.Dock = System.Windows.Forms.DockStyle.Fill;
      this.AboutLogo.ErrorImage = null;
      this.AboutLogo.Image = global::iMS.Properties.Resources.About;
      this.AboutLogo.InitialImage = null;
      this.AboutLogo.Location = new System.Drawing.Point(3, 3);
      this.AboutLogo.Name = "AboutLogo";
      this.tableLayoutPanel.SetRowSpan(this.AboutLogo, 6);
      this.AboutLogo.Size = new System.Drawing.Size(131, 259);
      this.AboutLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
      this.AboutLogo.TabStop = false;
      // 
      // ProductNameLabel
      // 
      this.ProductNameLabel.Dock = System.Windows.Forms.DockStyle.Fill;
      this.ProductNameLabel.Location = new System.Drawing.Point(143, 0);
      this.ProductNameLabel.Margin = new System.Windows.Forms.Padding(6, 0, 3, 0);
      this.ProductNameLabel.MaximumSize = new System.Drawing.Size(0, 17);
      this.ProductNameLabel.Name = "ProductNameLabel";
      this.ProductNameLabel.Size = new System.Drawing.Size(271, 17);
      this.ProductNameLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      this.ProductNameLabel.RightToLeft = Utils.Direction(ResourceManager.Resource.GetString("IsRightToLeft"));
      // 
      // Version
      // 
      this.Version.Dock = System.Windows.Forms.DockStyle.Fill;
      this.Version.Location = new System.Drawing.Point(143, 26);
      this.Version.Margin = new System.Windows.Forms.Padding(6, 0, 3, 0);
      this.Version.MaximumSize = new System.Drawing.Size(0, 17);
      this.Version.Name = "Version";
      this.Version.Size = new System.Drawing.Size(271, 17);
      this.Version.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      this.Version.RightToLeft = Utils.Direction(ResourceManager.Resource.GetString("IsRightToLeft"));
      // 
      // Copyright
      // 
      this.Copyright.Dock = System.Windows.Forms.DockStyle.Fill;
      this.Copyright.Location = new System.Drawing.Point(143, 52);
      this.Copyright.Margin = new System.Windows.Forms.Padding(6, 0, 3, 0);
      this.Copyright.MaximumSize = new System.Drawing.Size(0, 17);
      this.Copyright.Name = "Copyright";
      this.Copyright.Size = new System.Drawing.Size(271, 17);
      this.Copyright.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      this.Copyright.RightToLeft = Utils.Direction(ResourceManager.Resource.GetString("IsRightToLeft"));
      // 
      // CompanyNameLabel
      // 
      this.CompanyNameLabel.Dock = System.Windows.Forms.DockStyle.Fill;
      this.CompanyNameLabel.Location = new System.Drawing.Point(143, 78);
      this.CompanyNameLabel.Margin = new System.Windows.Forms.Padding(6, 0, 3, 0);
      this.CompanyNameLabel.MaximumSize = new System.Drawing.Size(0, 17);
      this.CompanyNameLabel.Name = "CompanyNameLabel";
      this.CompanyNameLabel.Size = new System.Drawing.Size(271, 17);
      this.CompanyNameLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      this.CompanyNameLabel.RightToLeft = Utils.Direction(ResourceManager.Resource.GetString("IsRightToLeft"));
      // 
      // Description
      // 
      this.Description.Dock = System.Windows.Forms.DockStyle.Fill;
      this.Description.Location = new System.Drawing.Point(143, 107);
      this.Description.Margin = new System.Windows.Forms.Padding(6, 3, 3, 3);
      this.Description.Multiline = true;
      this.Description.Name = "Description";
      this.Description.ReadOnly = true;
      this.Description.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
      this.Description.Size = new System.Drawing.Size(271, 126);
      this.Description.TabStop = false;
      this.Description.Text = ResourceManager.Resource.GetString(Constants.WORDS.XXVIII);
      this.Description.RightToLeft = Utils.Direction(ResourceManager.Resource.GetString("IsRightToLeft"));
      // 
      // OKButton
      // 
      this.OKButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
      this.OKButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
      this.OKButton.Location = new System.Drawing.Point(339, 239);
      this.OKButton.Name = "OKButton";
      this.OKButton.Size = new System.Drawing.Size(75, 23);
      this.OKButton.Text = ResourceManager.Resource.GetString(Constants.WORDS.XVI);
      this.OKButton.Click += new System.EventHandler(this.OK_Click);
      // 
      // aboutBox
      // 
      this.AcceptButton = this.OKButton;
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(435, 283);
      this.Controls.Add(this.tableLayoutPanel);
      this.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
      this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
      this.MaximizeBox = false;
      this.MinimizeBox = false;
      this.Name = "aboutBox";
      this.RightToLeft = Utils.Direction(ResourceManager.Resource.GetString("IsRightToLeft"));
      this.Padding = new System.Windows.Forms.Padding(9);
      this.ShowIcon = false;
      this.ShowInTaskbar = false;
      this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
      this.Text = ResourceManager.Resource.GetString(Constants.WORDS.XI);
      this.tableLayoutPanel.ResumeLayout(false);
      this.tableLayoutPanel.PerformLayout();
      ((System.ComponentModel.ISupportInitialize)(this.AboutLogo)).EndInit();
      this.ResumeLayout(false);
    }

    #endregion

    private System.Windows.Forms.TableLayoutPanel tableLayoutPanel;
    private System.Windows.Forms.PictureBox AboutLogo;
    private System.Windows.Forms.Label ProductNameLabel;
    private System.Windows.Forms.Label Version;
    private System.Windows.Forms.Label Copyright;
    private System.Windows.Forms.Label CompanyNameLabel;
    private System.Windows.Forms.TextBox Description;
    private System.Windows.Forms.Button OKButton;
  }
}